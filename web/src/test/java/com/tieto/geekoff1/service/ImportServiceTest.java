package com.tieto.geekoff1.service;

import static com.tieto.geekoff1.utils.PropertiesReader.getTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.tieto.geekoff1.service.Interfaces.DataImportServiceInterface;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/com/tieto/dao/applicationContext.xml")
@TransactionConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ImportServiceTest {

	@Autowired
	private DataImportServiceInterface dataImport;

	@Test
	@Rollback
	public void testReadFromResourceAndTokenizeToMeasurements() {

		List<String> allMeasurements = dataImport
				.readFromResourceAndTokenizeToMeasurements(getTest("dataUrl"));

		assertEquals("checking first data Token", getTest("dateCorrect1"),
				allMeasurements.get(0));
		assertEquals("checking last data Token", getTest("dataLastToken"),
				allMeasurements.get(allMeasurements.size() - 1));
		assertEquals(
				"checking 2nd date, which is at nr 6, because of our test.txt data",
				getTest("dateWrong1"), allMeasurements.get(5));
	}

	@Test
	@Rollback
	public void testSeparateMeasurementSets() {
		// tokenize
		List<String> allMeasurements = dataImport
				.readFromResourceAndTokenizeToMeasurements(getTest("dataUrl"));
		// make measurement sets
		List<List<String>> measurementSets = dataImport
				.separateMeasurementSets(allMeasurements);
		assertEquals(
				"because we have 1 wrong dateformat in our test.txt, we expect to see 3 measurement sets, instead of 4",
				3, measurementSets.size());
		int lastIndex = measurementSets.size() - 1;
		int lastElement = measurementSets.get(lastIndex).size() - 1;
		assertEquals(getTest("dateCorrect2"), measurementSets.get(lastIndex)
				.get(0));
		assertEquals(getTest("dataLastToken"), measurementSets.get(lastIndex)
				.get(lastElement));
	}

	@Test
	@Rollback
	public void separateOneMeasurementSetToMeasurementFirstCorrect() {
		// tokenize
		List<String> allMeasurements = dataImport
				.readFromResourceAndTokenizeToMeasurements(getTest("dataUrl"));
		// make measurement sets
		List<List<String>> measurementSets = dataImport
				.separateMeasurementSets(allMeasurements);
		// get first
		List<String> firstMeasurement = measurementSets.get(0);

		Map<String, String> firstMeasurementPairs = dataImport
				.separateOneMeasurementSetToMeasurement(firstMeasurement);
		assertEquals(getTest("dateCorrect1MS"),
				firstMeasurementPairs.get("measureDate"));
	}

	@Test
	@Rollback
	public void separateOneMeasurementSetToMeasurementSecondWrong() {
		// tokenize
		List<String> allMeasurements = dataImport
				.readFromResourceAndTokenizeToMeasurements(getTest("dataUrl"));
		// make measurement sets
		List<List<String>> measurementSets = dataImport
				.separateMeasurementSets(allMeasurements);
		// get second
		List<String> secondMeasurement = measurementSets.get(1);
		Map<String, String> secondMeasurementPairs = new HashMap<String, String>();
		try {
			secondMeasurementPairs = dataImport
					.separateOneMeasurementSetToMeasurement(secondMeasurement);
		} catch (Exception e) {
			secondMeasurementPairs = null;
		}
		assertNull(
				"measurementPairs cannot be created out of faulty data, (it creats exception, we replace result value with null",
				secondMeasurementPairs);
	}

	@Test
	@Rollback
	public void separateOneMeasurementSetToMeasurementThirdCorrect() {
		// tokenize
		List<String> allMeasurements = dataImport
				.readFromResourceAndTokenizeToMeasurements(getTest("dataUrl"));
		// make measurement sets
		List<List<String>> measurementSets = dataImport
				.separateMeasurementSets(allMeasurements);
		List<String> thirdMeasurement = measurementSets.get(2);
		Map<String, String> thirdMeasurementPairs = dataImport
				.separateOneMeasurementSetToMeasurement(thirdMeasurement);
		assertEquals(getTest("dateCorrect2MS"),
				thirdMeasurementPairs.get("measureDate"));
	}

}
