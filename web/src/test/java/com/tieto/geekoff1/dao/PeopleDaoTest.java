package com.tieto.geekoff1.dao;

import static com.tieto.geekoff1.utils.PropertiesReader.getTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/com/tieto/dao/applicationContext.xml")
@TransactionConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class PeopleDaoTest extends
		AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private PeopleDaoInterface peopleDao;
	@Autowired
	private GroupDaoInterface groupDao;

	@Test
	@Rollback
	public void testSaveNewPersonByNewPerson() {
		PersonsEntity person = new PersonsEntity();
		person.setFirstName(getTest("nameN"));
		person.setLastName(getTest("lastNameN"));
		person.setPersonId(getTest("idN"));
		peopleDao.saveNewPerson(person);
		assertEquals(
				person,
				peopleDao.getPersonByIdAndName(person.getPersonId(),
						person.getFirstName()));
	}

	@Test
	@Rollback
	public void testSaveNewPersonByExistingPerson() {
		PersonsEntity person = new PersonsEntity();
		person.setFirstName(getTest("nameE"));
		person.setLastName(getTest("lastNameE"));
		person.setPersonId(getTest("idE"));
		peopleDao.saveNewPerson(person);
		assertNull("because of duplicates 0 will be returned",
				peopleDao.getPersonByIdAndName((getTest("idE")),
						getTest("nameE")));
	}

	@Test
	@Rollback
	public void testGetPersonByIdAndName() {
		PersonsEntity person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		assertEquals(getTest("lastNameE"), person.getLastName());
	}

	@Test
	@Rollback
	public void testGetPersonByNameAndFamilyName() {
		PersonsEntity person = peopleDao.getPersonByNameAndFamilyName(
				getTest("nameE"), getTest("lastNameE"));
		assertEquals(getTest("idE"), person.getPersonId());
	}

	@Test
	@Rollback
	public void testSaveMeasurementToExistingPersonBySizeAndFeelAndMood() {
		PersonsEntity person = peopleDao.getPersonByNameAndFamilyName(
				getTest("nameE"), getTest("lastNameE"));
		int sizeBefore = person.getMeasurementsList().size();
		MeasurementsEntity measurement = new MeasurementsEntity();
		measurement.setMeasureDate(getTest("dateTime"));
		measurement.setFeel(getTest("feel"));
		measurement.setMood(getTest("mood"));
		// save
		peopleDao.saveMeasurementToExistingPerson(person, measurement);
		// reload
		person = null;
		measurement = null;
		person = peopleDao.getPersonByNameAndFamilyName(getTest("nameE"),
				getTest("lastNameE"));
		int sizeAfter = person.getMeasurementsList().size();
		assertEquals(sizeBefore + 1, sizeAfter);

		measurement = peopleDao
				.getMeasurementsEntityByDateTime(getTest("dateTime"));
		assertEquals(getTest("feel"), measurement.getFeel());
		assertEquals(getTest("mood"), measurement.getMood());

	}

	@Test
	@Rollback
	public void testGetAllPersonsListBySize() {
		int sizeBefore = peopleDao.getAllPersons().size();
		PersonsEntity person = new PersonsEntity();
		person.setFirstName(getTest("nameN"));
		person.setLastName(getTest("lastNameN"));
		person.setPersonId(getTest("idN"));
		peopleDao.saveNewPerson(person);
		int sizeAfter = peopleDao.getAllPersons().size();
		assertEquals(sizeBefore + 1, sizeAfter);
	}

	@Test
	@Rollback
	public void testSaveOrUpdateCommentToPerson() {
		PersonsEntity person = peopleDao.getPersonByNameAndFamilyName(
				getTest("nameE"), getTest("lastNameE"));
		person.setComment(getTest("commentE"));
		peopleDao.saveOrUpdateCommentToPerson(person.getPersonId(),
				person.getFirstName(), person.getComment());
		// reset
		person = null;
		person = peopleDao.getPersonByNameAndFamilyName(getTest("nameE"),
				getTest("lastNameE"));
		assertEquals(person.getComment(), getTest("commentE"));

	}

	@Test
	@Rollback
	public void testSaveOrUpdatePersonFamilyName() {
		PersonsEntity person = peopleDao.getPersonByNameAndFamilyName(
				getTest("nameE"), getTest("lastNameE"));
		person.setLastName(getTest("lastNameE2"));
		peopleDao.saveOrUpdateLastName(person.getPersonId(),
				person.getFirstName(), person.getLastName());
		// reset
		person = null;
		person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		assertEquals(person.getLastName(), getTest("lastNameE2"));
	}

	@Test
	@Rollback
	public void testSaveExistingGroupToPerson() {
		// create group
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		// getting person
		PersonsEntity person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		int sizeBefore = person.getGroups().size();
		peopleDao.saveExistingGroupToPerson(person, group);
		person = null;
		person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		int sizeAfter = person.getGroups().size();
		assertEquals(sizeBefore + 1, sizeAfter);
		assertEquals(
				"because person has groupList with only one groupName(the one we inserted), getting first one should return the one we inserted",
				getTest("groupName"), person.getGroupNames().get(0));
	}
}
