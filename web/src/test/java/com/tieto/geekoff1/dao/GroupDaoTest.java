package com.tieto.geekoff1.dao;

import static com.tieto.geekoff1.utils.PropertiesReader.getTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/com/tieto/dao/applicationContext.xml")
@TransactionConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class GroupDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private PeopleDaoInterface peopleDao;
	@Autowired
	private GroupDaoInterface groupDao;

	@Test
	@Rollback
	public void testSaveNewGroupByNewGroup() {
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		assertEquals(group, groupDao.getGroupByGroupName(getTest("groupName")));
	}

	@Test
	@Rollback
	public void testSaveNewGroupByExistingGroup() {
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		// reset
		group = null;
		group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);

		assertNull("because of duplicates 0 will be returned",
				groupDao.getGroupByGroupName(getTest("groupName")));
	}

	@Test
	@Rollback
	public void testGetGroupByName() {
		// create group because we have none in the DB
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		// reset
		group = null;
		group = groupDao.getGroupByGroupName(getTest("groupName"));
		assertEquals(getTest("groupName"), group.getGroupName());
	}

	@Test
	@Rollback
	public void testSaveMeasurementToExistingPersonBySizeAndFeelAndMood() {
		PersonsEntity person = peopleDao.getPersonByNameAndFamilyName(
				getTest("nameE"), getTest("lastNameE"));
		int sizeBefore = person.getMeasurementsList().size();
		MeasurementsEntity measurement = new MeasurementsEntity();
		measurement.setMeasureDate(getTest("dateTime"));
		measurement.setFeel(getTest("feel"));
		measurement.setMood(getTest("mood"));
		// save
		peopleDao.saveMeasurementToExistingPerson(person, measurement);
		// reload
		person = null;
		measurement = null;
		person = peopleDao.getPersonByNameAndFamilyName(getTest("nameE"),
				getTest("lastNameE"));
		int sizeAfter = person.getMeasurementsList().size();
		assertEquals(sizeBefore + 1, sizeAfter);

		measurement = peopleDao
				.getMeasurementsEntityByDateTime(getTest("dateTime"));
		assertEquals(getTest("feel"), measurement.getFeel());
		assertEquals(getTest("mood"), measurement.getMood());

	}

	@Test
	@Rollback
	public void testGetAllGroupsListBySize() {
		int sizeBefore = groupDao.getAllGroupNames().size();
		// create 2 groups (just because we always add 1, lets change the flow)
		GroupsEntity group2 = new GroupsEntity();
		group2.setGroupName(getTest("groupName2"));
		groupDao.saveNewGroup(group2);
		GroupsEntity group3 = new GroupsEntity();
		group3.setGroupName(getTest("groupName3"));
		groupDao.saveNewGroup(group3);
		int sizeAfter = groupDao.getAllGroupNames().size();
		assertEquals("we added 2 groups this time", sizeBefore + 2, sizeAfter);
	}

	@Test
	@Rollback
	public void testGetAllGroupPersonsByGroupName() {
		// create new group
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		// getting person
		PersonsEntity person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		// make 2nd person
		PersonsEntity person2 = new PersonsEntity();
		person2.setPersonId(getTest("idN"));
		person2.setFirstName(getTest("nameN"));
		peopleDao.saveNewPerson(person2);
		// add 2 persons to our group
		peopleDao.saveExistingGroupToPerson(person2, group);
		peopleDao.saveExistingGroupToPerson(person, group);
		// now lets test groupListSize
		int listSize = groupDao.getAllGroupPersonsbyGroupName(
				group.getGroupName()).size();
		assertEquals(2, listSize);
		// now test whether we have real objects
		// reset
		person = null;
		person2 = null;
		person = groupDao.getAllGroupPersonsbyGroupName(group.getGroupName())
				.get(0);
		person2 = groupDao.getAllGroupPersonsbyGroupName(group.getGroupName())
				.get(1);
		// list is sorted and we don't know how nameE compares to nameN
		int order = getTest("nameE").compareTo(getTest("nameN"));
		if (order < 0) {
			assertEquals(getTest("nameE"), person.getFirstName());
			assertEquals(getTest("nameN"), person2.getFirstName());
		} else {
			assertEquals(getTest("nameE"), person2.getFirstName());
			assertEquals(getTest("nameN"), person.getFirstName());
		}

	}

	@Test
	@Rollback
	public void testDeleteGroupFromPersonByAddingTwoRemovingOne() {
		// create new group
		GroupsEntity group = new GroupsEntity();
		group.setGroupName(getTest("groupName"));
		groupDao.saveNewGroup(group);
		// getting person
		PersonsEntity person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		// make 2nd person
		PersonsEntity person2 = new PersonsEntity();
		person2.setPersonId(getTest("idN"));
		person2.setFirstName(getTest("nameN"));
		person2.setLastName(getTest("lastNameN"));
		peopleDao.saveNewPerson(person2);
		// add 2 persons to our group
		peopleDao.saveExistingGroupToPerson(person, group);
		peopleDao.saveExistingGroupToPerson(person2, group);
		// remove 1
		groupDao.deleteGroupFromPerson(person, group);
		// reset
		person = null;
		person2 = null;
		// size should be 1
		assertEquals(1,
				groupDao.getAllGroupPersonsbyGroupName(getTest("groupName"))
						.size());
		// second person should have same name
		person2 = groupDao.getAllGroupPersonsbyGroupName(getTest("groupName"))
				.get(0);
		assertEquals(getTest("nameN"), person2.getFirstName());
		// person (1) should not have a group, while person2 should have one
		person = peopleDao.getPersonByIdAndName(getTest("idE"),
				getTest("nameE"));
		person2 = peopleDao.getPersonByIdAndName(getTest("idN"),
				getTest("nameN"));
		int personGroupSize = person.getGroupNames().size();
		String person2GroupName = person2.getGroupNames().get(0);
		assertEquals("person from whom we deleted the group has no groups", 0,
				personGroupSize);
		assertEquals(getTest("groupName"), person2GroupName);

	}
}
