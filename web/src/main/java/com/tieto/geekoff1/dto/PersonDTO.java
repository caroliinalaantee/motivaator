package com.tieto.geekoff1.dto;

import java.util.List;

public class PersonDTO {

	private String firstName;
	private String lastName;
	private String idNumber;
	private List<String> groupList;
	private String comment;

	public PersonDTO() {
	}

	public PersonDTO(String firstName, String lastName, String id,
			List<String> groupList) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.idNumber = id;
		this.groupList = groupList;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public List<String> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<String> groupList) {
		this.groupList = groupList;
	}

	@Override
	public String toString() {
		return "PersonDTO [firstName=" + firstName + ", lastName=" + lastName
				+ ", idNumber=" + idNumber + ", groupList=" + groupList
				+ ", comment=" + comment + "]";
	}
}
