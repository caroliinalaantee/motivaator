package com.tieto.geekoff1.dto;

import java.util.List;

public class PersonWithMeasurementsDTO {

	private PersonDTO person;
	private MeasurementsDTO measurements;

	public PersonWithMeasurementsDTO() {
	}

	public PersonWithMeasurementsDTO(PersonDTO person,
			MeasurementsDTO measurements) {
		this.person = person;
		this.measurements = measurements;
	}

	public PersonDTO getPerson() {
		return person;
	}

	public void setPerson(PersonDTO person) {
		this.person = person;
	}

	public MeasurementsDTO getMeasurements() {
		return measurements;
	}

	public void setMeasurements(MeasurementsDTO measurements) {
		this.measurements = measurements;
	}

	@Override
	public String toString() {
		return "PersonWithMeasurementsDTO [person=" + person
				+ ", measurements=" + measurements + "]";
	}
}
