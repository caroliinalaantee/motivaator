package com.tieto.geekoff1.dto;

import java.util.Calendar;
import java.util.Date;

public class MeasurementDTO implements Comparable<MeasurementDTO>{

	private String date;
	private String value;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "MeasurementDTO [date=" + date + ", value=" + value + "]";
	}
	
	public String stringForHtml() {
		return this.date + "," + this.value;
	}
	
	public int compareTo(MeasurementDTO m) {	
		return this.date.compareTo(m.getDate());
	}

	
}
