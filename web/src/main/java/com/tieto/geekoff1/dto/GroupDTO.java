package com.tieto.geekoff1.dto;

import java.util.List;

public class GroupDTO {

	private String groupName;
	private String groupDescription;
	private List<PersonWithMeasurementsDTO> groupMembers;

	public GroupDTO() {

	}

	public GroupDTO(String name, String description) {
		this.groupName = name;
		this.groupDescription = description;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public List<PersonWithMeasurementsDTO> getGroupMembers() {
		return groupMembers;
	}

	public void setGroupMembers(List<PersonWithMeasurementsDTO> groupMembers) {
		this.groupMembers = groupMembers;
	}

	@Override
	public String toString() {
		return "GroupDTO [groupName=" + groupName + ", groupDescription="
				+ groupDescription + ", groupMembers=" + groupMembers + "]";
	}
}
