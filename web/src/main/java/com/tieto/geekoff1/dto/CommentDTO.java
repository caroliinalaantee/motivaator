package com.tieto.geekoff1.dto;

import java.util.Date;

public class CommentDTO {

	private Date date;
	private String commentText;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	@Override
	public String toString() {
		return "CommentDTO [date=" + date + ", commentText=" + commentText
				+ "]";
	}
}
