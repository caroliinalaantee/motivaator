package com.tieto.geekoff1.dto;

import java.util.ArrayList;
import java.util.List;

public class MeasurementsDTO {

	private List<MeasurementDTO> feeling;
	private List<MeasurementDTO> mood;
	private List<MeasurementDTO> weight;
	private List<MeasurementDTO> pulse;
	private List<MeasurementDTO> bloodOxygen;
	private List<MeasurementDTO> height;
	private List<MeasurementDTO> upperBloodPressure;
	private List<MeasurementDTO> lowerBloodPressure;
	private List<MeasurementDTO> bmi;

	public MeasurementsDTO() {
		feeling = new ArrayList<MeasurementDTO>();
		mood = new ArrayList<MeasurementDTO>();
		weight = new ArrayList<MeasurementDTO>();
		pulse = new ArrayList<MeasurementDTO>();
		bloodOxygen = new ArrayList<MeasurementDTO>();
		height = new ArrayList<MeasurementDTO>();
		upperBloodPressure = new ArrayList<MeasurementDTO>();
		lowerBloodPressure = new ArrayList<MeasurementDTO>();
		bmi = new ArrayList<MeasurementDTO>();
	}

	public List<MeasurementDTO> getFeeling() {
		return feeling;
	}

	public void setFeeling(List<MeasurementDTO> feeling) {
		this.feeling = feeling;
	}

	public List<MeasurementDTO> getMood() {
		return mood;
	}

	public void setMood(List<MeasurementDTO> mood) {
		this.mood = mood;
	}

	public List<MeasurementDTO> getWeight() {
		return weight;
	}

	public void setWeight(List<MeasurementDTO> weight) {
		this.weight = weight;
	}

	public List<MeasurementDTO> getPulse() {
		return pulse;
	}

	public void setPulse(List<MeasurementDTO> pulse) {
		this.pulse = pulse;
	}

	public List<MeasurementDTO> getBloodOxygen() {
		return bloodOxygen;
	}

	public void setBloodOxygen(List<MeasurementDTO> bloodOxygen) {
		this.bloodOxygen = bloodOxygen;
	}

	public List<MeasurementDTO> getHeight() {
		return height;
	}

	public void setHeight(List<MeasurementDTO> height) {
		this.height = height;
	}

	public List<MeasurementDTO> getUpperBloodPressure() {
		return upperBloodPressure;
	}

	public void setUpperBloodPressure(List<MeasurementDTO> upperBloodPressure) {
		this.upperBloodPressure = upperBloodPressure;
	}

	public List<MeasurementDTO> getLowerBloodPressure() {
		return lowerBloodPressure;
	}

	public void setLowerBloodPressure(List<MeasurementDTO> lowerBloodPressure) {
		this.lowerBloodPressure = lowerBloodPressure;
	}

	public List<MeasurementDTO> getBmi() {
		return bmi;
	}

	public void setBmi(List<MeasurementDTO> bmi) {
		this.bmi = bmi;
	}

	@Override
	public String toString() {
		return "MeasurementsDTO [feeling=" + feeling + ", mood=" + mood
				+ ", weight=" + weight + ", pulse=" + pulse + ", bloodOxygen="
				+ bloodOxygen + ", height=" + height + ", upperBloodPressure="
				+ upperBloodPressure + ", lowerBloodPressure="
				+ lowerBloodPressure + ", bmi=" + bmi + "]";
	}
}
