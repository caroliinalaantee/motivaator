package com.tieto.geekoff1.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PropertiesReader {

	@Autowired
	private static Logger logger;

	public static String get(String key) {
		Properties properties = new Properties();
		String value = "unsuccessful load";
		try {
			InputStream inputStream = PropertiesReader.class.getClassLoader()
					.getResourceAsStream("paths.properties");

			properties.load(inputStream);
			value = properties.getProperty(key);
		} catch (IOException e) {
			logger.error("PropertiesReader error: " + e);
		}
		return value;
	}

	public static String getTest(String key) {
		Properties properties = new Properties();
		String value = "unsuccessful load";
		try {
			InputStream inputStream = PropertiesReader.class.getClassLoader()
					.getResourceAsStream("testDatapaths.properties");

			properties.load(inputStream);
			value = properties.getProperty(key);
		} catch (IOException e) {
			logger.error("PropertiesReader error: " + e);
		}
		return value;
	}
}
