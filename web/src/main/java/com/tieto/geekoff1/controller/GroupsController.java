package com.tieto.geekoff1.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Controller
public class GroupsController {

	@Autowired
	private GroupServiceInterface groupService;

	@Autowired
	private PeopleServiceInterface peopleService;

	@RequestMapping(value = "/groups", method = RequestMethod.GET)
	public String getRecords(Model model) {
		addGroupNameList(model);
		model.addAttribute("namesList", getNameList());

		addSearchList(model);
		return "groups";
	}

	private void addSearchList(Model model) {
		List<String> searchList = groupService.getAllGroupNames();
		searchList.addAll(getNameList());
		model.addAttribute("searchList", searchList);
	}

	public List<String> getNameList() {
		List<PersonDTO> people = peopleService.getAllPersons();
		List<String> names = new ArrayList<String>();
		for (PersonDTO p : people) {
			names.add(getFullname(p));
		}
		return names;
	}

	public String getFullname(PersonDTO person) {
		return person.getFirstName() + " " + person.getLastName();
	}

	private void addGroupNameList(Model model) {
		model.addAttribute("groupsList", groupService.getAllGroupNames());
	}
}
