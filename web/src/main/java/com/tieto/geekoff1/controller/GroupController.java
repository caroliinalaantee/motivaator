package com.tieto.geekoff1.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.dto.GroupDTO;
import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;
import com.tieto.geekoff1.service.Interfaces.UtilityServiceInterface;

@Controller
public class GroupController {

	@Autowired
	private GroupServiceInterface groupService;

	@Autowired
	private UtilityServiceInterface utilityService;

	@Autowired
	private PeopleServiceInterface peopleService;

	@RequestMapping(value = "/group", method = RequestMethod.GET)
	public String getRecords(
			@RequestParam(value = "name", required = false) String groupName,
			Model model) {
		addGroupNameList(model);
		model.addAttribute("groupName", groupName);
		model.addAttribute("namesList", getNameList());
		addSearchList(model);
		try{
			addGroupInfo(model, groupName);
		} catch (Exception e) {
			e.printStackTrace();
			return "url_fail";
		}
		addGroupData(model, groupName);
		return "group";
	}

	private void addSearchList(Model model) {
		List<String> searchList = groupService.getAllGroupNames();
		searchList.addAll(getNameList());
		model.addAttribute("searchList", searchList);
	}

	public List<String> getNameList() {
		List<PersonDTO> people = peopleService.getAllPersons();
		List<String> names = new ArrayList<String>();
		for (PersonDTO person : people) {
			names.add(getFullName(person));
		}
		return names;
	}

	public String getFullName(PersonDTO person) {
		return person.getFirstName() + " " + person.getLastName();
	}

	private void addGroupInfo(Model model, String groupName) {
		GroupDTO group = groupService.getGroupDTOByName(groupName);
		List<String> groupMemberStringList = new ArrayList<String>();
		int count = 0;
		for (PersonWithMeasurementsDTO person : group.getGroupMembers()) {
			groupMemberStringList.add(getFullName(person.getPerson()));
			count++;
		}
		model.addAttribute("peopleList", groupMemberStringList);
		model.addAttribute("peopleCount", count);
	}
	
	private void addGroupNameList(Model model) {
		model.addAttribute("groupsList", groupService.getAllGroupNames());
	}
	
	private void addGroupData(Model model, String groupName) {
		model.addAttribute("groupMoodData", getMeasurementDataString("mood", groupName)); 
		model.addAttribute("groupFeelingData", getMeasurementDataString("feeling", groupName)); 
		model.addAttribute("groupWeightData", getMeasurementDataString("weight", groupName)); 
		model.addAttribute("groupPulseData", getMeasurementDataString("pulse", groupName)); 
		model.addAttribute("groupBloodOxygenData", getMeasurementDataString("bloodOxygen", groupName)); 
		model.addAttribute("lowerBloodPressure", getMeasurementDataString("lowerBloodPressure", groupName));
		model.addAttribute("upperBloodPressure", getMeasurementDataString("upperBloodPressure", groupName));
	}
	
	private String getMeasurementDataString(String measurementName, String groupName){
		List<MeasurementDTO> measurements = utilityService.getAverageMeasurementListOverPeriod(groupName, measurementName);
		List<String> data = new ArrayList<String>();
		for (MeasurementDTO m : measurements) {
			data.add(m.stringForHtml());
		}
		return data.toString();
	}
}
