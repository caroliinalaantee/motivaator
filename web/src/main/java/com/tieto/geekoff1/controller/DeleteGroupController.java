package com.tieto.geekoff1.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Controller
public class DeleteGroupController {

	@Autowired
	@Qualifier("peopleService")
	private PeopleServiceInterface peopleService;
	@Autowired
	private GroupServiceInterface groupService;
	@Autowired
	private Logger logger;

	@RequestMapping(value = "/deleteGroup", method = RequestMethod.GET)
	public String addLabel(
			@RequestHeader("Referer") String referer,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "groupName", required = true) String groupName,
			@RequestParam(value = "firstName", required = true) String firstName) {
		if (id == null && groupName == null) {
			logger.error("Problem in DeleteGroupController, id or groupName are null");
		}
		logger.debug("Hello from DeleteGroupController. Will use groupService.deleteGroupFromPerson. Entering in:"
				+ id + " " + firstName + " " + groupName);
		groupService.deleteGroupFromPerson(id, firstName, groupName);
		return "redirect:" + referer;
	}

}