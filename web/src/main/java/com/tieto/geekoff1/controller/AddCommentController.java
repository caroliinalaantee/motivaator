package com.tieto.geekoff1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Controller
public class AddCommentController {

	@Autowired
	@Qualifier("peopleService")
	private PeopleServiceInterface peopleService;

	@RequestMapping(value = "/savecomment", method = RequestMethod.GET)
	public String addComment(
			@RequestHeader("Referer") String referer,
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "comment", required = false) String comment,
			@RequestParam(value = "firstName", required = false) String firstName) {
		peopleService.saveCommentToPerson(id, firstName, comment);
		referer = referer + "#showComments";
		return "redirect:" + referer;
	}

}