package com.tieto.geekoff1.dao.interfaces;

import java.util.List;

import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

/**
 * PeopleDaoInterface class contains methods required to pass data between
 * service and domain layer
 */
public interface PeopleDaoInterface {

	/**
	 * getPersonByNameAndFamilyName method finds a person by first name and last
	 * name
	 * 
	 * @param name
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 * @return - PersonsEntity object
	 */
	public PersonsEntity getPersonByNameAndFamilyName(String name,
			String lastName);

	/**
	 * getPersonByIdAndName method finds a person by personal code and first
	 * name
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's first name
	 * @return PersonsEntity type object
	 */
	public PersonsEntity getPersonByIdAndName(String id, String name);

	/**
	 * getMeasurementsEntityByDateTime method is required for data import, to
	 * check what measurement of the same date already exists
	 * 
	 * @param dateTime
	 *            - dateTime (in milliseconds)
	 * @return
	 */
	public MeasurementsEntity getMeasurementsEntityByDateTime(String dateTime);

	/**
	 * getAllPersons method gets all persons
	 * 
	 * @return PersonsEntity type List
	 */
	public List<PersonsEntity> getAllPersons();

	/**
	 * getPersonMeasurementsByPersonName method finds all measurements of person
	 * 
	 * @param name
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 * @return MeasurementsEntity type List
	 */
	public List<MeasurementsEntity> getPersonMeasurementsByPersonName(
			String name, String lastName);

	/**
	 * saveNewPerson method saves a new person to database
	 * 
	 * @param person
	 *            - PersonsEntity type object
	 */
	public void saveNewPerson(PersonsEntity person);

	/**
	 * saveMeasurementToExistingPerson method saves measurement set to person
	 * 
	 * @param person
	 *            - PersonsEntity type object
	 * @param measurements
	 *            - MeasurementsEntity type object
	 */
	public void saveMeasurementToExistingPerson(PersonsEntity person,
			MeasurementsEntity measurements);

	/**
	 * saveExistingGroupToPerson method adds person to group
	 * 
	 * @param person
	 *            - PersonsEntity type object
	 * @param group
	 *            - GroupsEnitity type object
	 */
	public void saveExistingGroupToPerson(PersonsEntity person,
			GroupsEntity group);

	/**
	 * saveOrUpdateCommentToPerson method updates comment of person
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's first name
	 * @param comment
	 *            - comment of person
	 */
	public void saveOrUpdateCommentToPerson(String id, String name,
			String comment);

	/**
	 * saveOrUpdateLastName method updates person's last name
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 */
	public void saveOrUpdateLastName(String id, String name, String lastName);
}
