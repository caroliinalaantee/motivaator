package com.tieto.geekoff1.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

@Component
public class GroupDAO implements GroupDaoInterface {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private PeopleDaoInterface peopleDao;
	@Autowired
	private Logger logger;

	public GroupsEntity getGroupByGroupName(String name) {
		logger.debug("hello from GroupDAO. Activated getGroupByGroupName. name to search is "
				+ name);

		GroupsEntity groupEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			groupEntity = (GroupsEntity) session
					.createCriteria(GroupsEntity.class)
					.add(Restrictions.eq("groupName", name)).uniqueResult();
			// .setFetchMode("personsList", FetchMode.JOIN).uniqueResult();
		} catch (HibernateException e) {
			logger.error("Unable to complete GroupDAO getGroupByGroupName"
					+ e.getMessage());
		}
		return groupEntity;
	}

	public void saveNewGroup(GroupsEntity group) {
		logger.debug("hello from GroupDAO. Activated saveNewGroup. Saving group to the database");
		Session session = sessionFactory.getCurrentSession();
		session.save(group);
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllGroupNames() {
		logger.debug("hello from GroupDAO. Activated getAllGroupNames. Getting all group names");
		Set<GroupsEntity> groupsEntitySet = new HashSet<GroupsEntity>();
		List<GroupsEntity> groupsEntityList = new ArrayList<GroupsEntity>();

		Session session = sessionFactory.getCurrentSession();

		try {
			groupsEntityList = session.createCriteria(GroupsEntity.class)
					.list();
		} catch (HibernateException e) {
			logger.error("Unable to complete GroupDAO getAllGroupNames"
					+ e.getMessage());
		}

		// list-> set -> list + sort conversion
		groupsEntitySet = new HashSet<GroupsEntity>(groupsEntityList);
		groupsEntityList = new ArrayList<GroupsEntity>(groupsEntitySet);
		Collections.sort(groupsEntityList);

		List<String> allGroupNames = new ArrayList<String>();
		for (GroupsEntity entity : groupsEntityList) {
			allGroupNames.add(entity.getGroupName());
		}
		return allGroupNames;
	}

	@SuppressWarnings("unchecked")
	public List<PersonsEntity> getAllGroupPersonsbyGroupName(String groupName) {
		logger.debug("hello from DAO. Activated getAllGroupPersonsbyGroupName. Getting all group persons by group name: "
				+ groupName);

		Set<PersonsEntity> personsSet = new HashSet<PersonsEntity>();
		List<PersonsEntity> personsList = new ArrayList<PersonsEntity>();

		Session session = sessionFactory.getCurrentSession();
		try {
			personsList = (session.createCriteria(PersonsEntity.class)
					.createAlias("groups", "group")
					.add(Restrictions.eq("group.groupName", groupName)).list());

		} catch (HibernateException e) {
			logger.error("Unable to complete GroupDAO getAllGroupPersonsbyGroupName"
					+ e.getMessage());
		}

		// list-> set -> list + sort conversion
		personsSet = new HashSet<PersonsEntity>(personsList);
		personsList = new ArrayList<PersonsEntity>(personsSet);
		Collections.sort(personsList);
		return personsList;
	}

	public void deleteGroupFromPerson(PersonsEntity person, GroupsEntity group) {
		logger.debug("hello from GroupDAO. Activated deleteGroupFromPerson. Deleting connection between: "
				+ person.getFirstName() + " " + group.getGroupName());
		// Session session = sessionFactory.getCurrentSession();

		boolean isRemoved = group.getPersonsList().remove(person);
		logger.debug("isRemoved " + isRemoved);
	}
}