package com.tieto.geekoff1.dao.interfaces;

import java.util.List;

import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

/**
 * GroupDaoInterface class contains methods required to pass data between
 * service and domain layer
 */
public interface GroupDaoInterface {

	/**
	 * getGroupByGroupName method gets a single GroupsEntity
	 * 
	 * @param name
	 *            - group name
	 * @return GroupsEntity type object
	 */
	public GroupsEntity getGroupByGroupName(String name);

	/**
	 * getAllGroupNames method gets all group names
	 * 
	 * @return Group name List of String type
	 */
	public List<String> getAllGroupNames();

	/**
	 * getAllGroupPersonsbyGroupName method gets all persons inside group
	 * 
	 * @param groupName
	 *            - group name
	 * @return List of PersonsEntity type
	 */
	public List<PersonsEntity> getAllGroupPersonsbyGroupName(String groupName);

	/**
	 * saveNewGroup creates new group
	 * 
	 * @param group
	 *            - GroupsEntity type object
	 */
	public void saveNewGroup(GroupsEntity group);

	/**
	 * deleteGroupFromPerson method removes person from group
	 * 
	 * @param person
	 *            - PersonsEntity type object
	 * @param group
	 *            - GroupsEntity type object
	 */
	public void deleteGroupFromPerson(PersonsEntity person, GroupsEntity group);

}
