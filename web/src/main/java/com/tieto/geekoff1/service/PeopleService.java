package com.tieto.geekoff1.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Service("peopleService")
@Transactional
public class PeopleService implements PeopleServiceInterface {

	@Autowired
	private PeopleDaoInterface peopleDao;
	@Autowired
	private GroupDaoInterface groupDao;
	@Autowired
	private Logger logger;

	public PersonDTO getPersonByNameAndFamilyName(String name, String lastName) {

		PersonDTO person = new PersonDTO();
		PersonsEntity personsEntity = peopleDao.getPersonByNameAndFamilyName(
				name, lastName);

		person.setFirstName(personsEntity.getFirstName());
		person.setLastName(personsEntity.getLastName());
		person.setIdNumber(personsEntity.getPersonId());
		person.setGroupList(personsEntity.getGroupNames());
		person.setComment(personsEntity.getComment());

		return person;
	}

	public List<PersonDTO> getAllPersons() {

		List<PersonDTO> persons = new ArrayList<PersonDTO>();
		List<PersonsEntity> personsEntityList = peopleDao.getAllPersons();

		PersonDTO person;

		for (PersonsEntity personsEntity : personsEntityList) {
			person = new PersonDTO();
			person.setFirstName(personsEntity.getFirstName());
			person.setLastName(personsEntity.getLastName());
			person.setIdNumber(personsEntity.getPersonId());
			persons.add(person);
		}

		return persons;
	}

	public PersonWithMeasurementsDTO getPersonWithMeasurementsByName(
			String firstName, String lastName) {

		PersonWithMeasurementsDTO personWithMM = new PersonWithMeasurementsDTO();

		PersonDTO person = this.getPersonByNameAndFamilyName(firstName,
				lastName);
		MeasurementsDTO measurements = this.getAllMeasurementsByPerson(person);
		personWithMM.setPerson(person);
		personWithMM.setMeasurements(measurements);
		return personWithMM;

	}

	public MeasurementsDTO getAllMeasurementsByPerson(PersonDTO person) {

		MeasurementsDTO measurementsDTO = new MeasurementsDTO();
		List<MeasurementsEntity> measurementsEntityList = peopleDao
				.getPersonMeasurementsByPersonName(person.getFirstName(),
						person.getLastName());

		for (MeasurementsEntity measurementsEntity : measurementsEntityList) {
			String date = measurementsEntity.getMeasureDate();

			String feeling = measurementsEntity.getFeel();
			this.addMeasurementEntitysToMeasurementsDTO("feeling", feeling,
					date, measurementsDTO);

			String mood = measurementsEntity.getMood();
			this.addMeasurementEntitysToMeasurementsDTO("mood", mood, date,
					measurementsDTO);

			String weight = measurementsEntity.getWeight();
			this.addMeasurementEntitysToMeasurementsDTO("weight", weight, date,
					measurementsDTO);

			String pulse = measurementsEntity.getPulse();
			this.addMeasurementEntitysToMeasurementsDTO("pulse", pulse, date,
					measurementsDTO);

			String bloodOxygen = measurementsEntity.getSpo2();
			this.addMeasurementEntitysToMeasurementsDTO("bloodOxygen",
					bloodOxygen, date, measurementsDTO);

			String height = measurementsEntity.getHeight();
			this.addMeasurementEntitysToMeasurementsDTO("height", feeling,
					date, measurementsDTO);

			String upperBloodPressure = measurementsEntity.getSys();
			this.addMeasurementEntitysToMeasurementsDTO("upperBloodPressure",
					upperBloodPressure, date, measurementsDTO);

			String lowerBloodPressure = measurementsEntity.getDia();
			this.addMeasurementEntitysToMeasurementsDTO("lowerBloodPressure",
					lowerBloodPressure, date, measurementsDTO);

			String bmi = measurementsEntity.getBmi();
			this.addMeasurementEntitysToMeasurementsDTO("bmi", bmi, date,
					measurementsDTO);

		}
		return measurementsDTO;
	}

	private void addMeasurementEntitysToMeasurementsDTO(String devicename,
			String measurement, String date, MeasurementsDTO measurementsDTO) {
		if (devicename.equals("feeling") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getFeeling().add(singleMeasurementDTO);
		}

		else if (devicename.equals("mood") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getMood().add(singleMeasurementDTO);
		}

		else if (devicename.equals("weight") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getWeight().add(singleMeasurementDTO);
		}

		else if (devicename.equals("pulse") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getPulse().add(singleMeasurementDTO);
		}

		else if (devicename.equals("bloodOxygen") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getBloodOxygen().add(singleMeasurementDTO);
		}

		else if (devicename.equals("height") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getHeight().add(singleMeasurementDTO);
		}

		else if (devicename.equals("upperBloodPressure") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getUpperBloodPressure().add(singleMeasurementDTO);
		}

		else if (devicename.equals("lowerBloodPressure") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getLowerBloodPressure().add(singleMeasurementDTO);
		}

		else if (devicename.equals("bmi") && measurement != null) {
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getBmi().add(singleMeasurementDTO);
		}

	}

	public PersonsEntity createNewPerson(String personId, String firstName) {

		PersonsEntity person = new PersonsEntity();
		person.setPersonId(personId);
		person.setFirstName(firstName);
		person.setLastName("Teadmata");
		peopleDao.saveNewPerson(person);
		return person;
	}

	public PersonsEntity getPersonByIdAndName(String personId, String firstName) {

		PersonsEntity person = peopleDao.getPersonByIdAndName(personId,
				firstName);
		if (person == null) {
			person = createNewPerson(personId, firstName);
			return person;
		}
		return person;
	}

	public void saveMeasurements(PersonsEntity person,
			MeasurementsEntity measurements) {
		peopleDao.saveMeasurementToExistingPerson(person, measurements);

	}

	public GroupsEntity addGroupToPerson(String personId, String firstName,
			String label) {

		GroupsEntity group = groupDao.getGroupByGroupName(label);
		if (group == null) {
			group = new GroupsEntity();
			group.setGroupName(label);
			groupDao.saveNewGroup(group);

			peopleDao.saveExistingGroupToPerson(
					peopleDao.getPersonByIdAndName(personId, firstName), group);

			return group;
		} else {
			peopleDao.saveExistingGroupToPerson(
					peopleDao.getPersonByIdAndName(personId, firstName), group);
		}

		return group;
	}

	public void saveCommentToPerson(String id, String name, String comment) {
		if (id == null || name == null || comment == null) {
			logger.debug("PeopleControllerService:saveCommentToPerson: id, nimi v�i comment on null!");
		} else {
			peopleDao.saveOrUpdateCommentToPerson(id, name, comment);
		}

	}

	public void saveLastNameToPerson(String id, String name, String lastName) {
		if (id == null || name == null || lastName == null) {
			logger.debug("PeopleControllerService:saveLastNameToPerson: id, nimi v�i comment on null!");
		} else {
			peopleDao.saveOrUpdateLastName(id, name, lastName);
		}

	}

	public PersonDTO getPersonDTOByPersonEntity(PersonsEntity personE) {
		if (personE != null) {
			PersonDTO personDTO = new PersonDTO();

			personDTO.setFirstName(personE.getFirstName());
			personDTO.setLastName(personE.getLastName());
			personDTO.setIdNumber(personE.getPersonId());
			personDTO.setComment(personE.getComment());

			return personDTO;
		}
		return null;
	}

}
