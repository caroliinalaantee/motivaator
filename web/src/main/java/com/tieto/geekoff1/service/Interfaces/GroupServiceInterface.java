package com.tieto.geekoff1.service.Interfaces;

import java.util.List;

import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.GroupDTO;

/**
 * getPersonDTOByPersonEntity class contains required service methods of groups
 */
public interface GroupServiceInterface {

	/**
	 * getGroupByGroupName method finds a group name from GROUP table
	 * 
	 * @param groupName
	 *            - group name
	 * @return GroupsEntity type object
	 */
	public GroupsEntity getGroupByGroupName(String groupName);

	/**
	 * getAllGroupPersonsbyGroupName method finds all persons inside group
	 * 
	 * @param groupName
	 *            - group name
	 * @return PersonsEntity type List
	 */
	public List<PersonsEntity> getAllGroupPersonsbyGroupName(String groupName);

	/**
	 * createNewGroup method creates a new group inside GROUPS table
	 * 
	 * @param label
	 *            - group name
	 * @return GroupsEntity type object
	 */
	public GroupsEntity createNewGroup(String label);

	/**
	 * getAllGroupNames method finds all group names from GROUP table
	 * 
	 * @return String type List
	 */
	public List<String> getAllGroupNames();

	/**
	 * getGroupDTOByName method finds GropuDTO by group name
	 * 
	 * @param name
	 *            - group name
	 * @return GroupDTO type object
	 */
	public GroupDTO getGroupDTOByName(String name);

	/**
	 * deleteGroupFromPerson method removes a person from group
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's name
	 * @param groupName
	 *            - group name
	 */
	public void deleteGroupFromPerson(String id, String name, String groupName);

}
