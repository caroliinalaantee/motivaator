package com.tieto.geekoff1.service.Interfaces;

import java.util.List;

import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;

/**
 * PeopleService class contains required service methods of persons
 */
public interface PeopleServiceInterface {

	/**
	 * getPersonByNameAndFamilyName method finds a person from table PERSON
	 * 
	 * @param name
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 * @return PersonDTO type object
	 */
	public PersonDTO getPersonByNameAndFamilyName(String name, String lastName);

	/**
	 * getAllPersons method finds all persons from table PERSON
	 * 
	 * @return PersonDTO type List
	 */
	public List<PersonDTO> getAllPersons();

	/**
	 * getPersonWithMeasurementsByName method finds one type measurement (e.g.
	 * pulse) of person from table PERSON and MEASUREMENT
	 * 
	 * @param firstName
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 * @return PersonWithMeasurementsDTO type object
	 */
	public PersonWithMeasurementsDTO getPersonWithMeasurementsByName(
			String firstName, String lastName);

	/**
	 * getAllMeasurementsByPerson method finds all measurements of person from
	 * table PERSON and MEASUREMENT
	 * 
	 * @param person
	 *            - PersonDTO type object
	 * @return MeasurementsDTO type object
	 */
	public MeasurementsDTO getAllMeasurementsByPerson(PersonDTO person);

	/**
	 * createNewPerson method creates a new person inside table PERSON
	 * 
	 * @param personId
	 *            - personal code
	 * @param firstName
	 *            - person's first name
	 * @return PersonsEntity type object
	 */
	public PersonsEntity createNewPerson(String personId, String firstName);

	/**
	 * getPersonByIdAndName method finds a person from table PERSON
	 * 
	 * @param personId
	 *            - personal code
	 * @param firstName
	 *            - person's last name
	 * @return PersonsEntity type object
	 */
	public PersonsEntity getPersonByIdAndName(String personId, String firstName);

	/**
	 * saveMeasurements method saves measurements of person to table MEASRUEMENT
	 * 
	 * @param person
	 *            - PersonsEntity type object
	 * @param measurements
	 *            - MeasurementsEntity type object
	 */
	public void saveMeasurements(PersonsEntity person,
			MeasurementsEntity measurements);

	/**
	 * addGroupToPerson method adds person to group
	 * 
	 * @param personId
	 *            - personal code
	 * @param firstName
	 *            - person's first name
	 * @param groupName
	 *            - person's last name
	 * @return GroupsEntity type object
	 */
	public GroupsEntity addGroupToPerson(String personId, String firstName,
			String groupName);

	/**
	 * saveCommentToPerson method updates a comment of person to table PERSON
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's first name
	 * @param comment
	 *            - person's last name
	 */
	public void saveCommentToPerson(String id, String name, String comment);

	/**
	 * saveLastNameToPerson method updates a last name of person
	 * 
	 * @param id
	 *            - personal code
	 * @param name
	 *            - person's first name
	 * @param lastName
	 *            - person's last name
	 */
	public void saveLastNameToPerson(String id, String name, String lastName);

	/**
	 * getPersonDTOByPersonEntity method finds PersonDTO object of person
	 * 
	 * @param personE
	 *            - PersonsEntity type object
	 * @return PersonDTO type object
	 */
	public PersonDTO getPersonDTOByPersonEntity(PersonsEntity personE);
}