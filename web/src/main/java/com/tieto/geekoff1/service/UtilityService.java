package com.tieto.geekoff1.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;
import com.tieto.geekoff1.service.Interfaces.UtilityServiceInterface;

@Service
public class UtilityService implements UtilityServiceInterface {

	@Autowired
	private GroupDaoInterface groupDao;

	@Autowired
	private PeopleDaoInterface peopleDao;

	@Autowired
	private PeopleServiceInterface peopleService;

	@Autowired
	private GroupServiceInterface groupService;

	@Autowired
	private Logger logger;

	public List<MeasurementDTO> getAverageMeasurementListOverPeriod(
			String groupName, String measurementName) {

		// get List of all persons belonging to group specified by groupName
		List<PersonsEntity> personsEntityList = groupService
				.getAllGroupPersonsbyGroupName(groupName);

		// check if personsEntityList is not null
		if (personsEntityList == null) {
			logger.debug("personsEntityList==null");
			return null;
		}

		// create new list for containing all measurements with type specified
		// by measurementName
		List<MeasurementDTO> listOfAllPersonsMeasurementDTOs = new LinkedList<MeasurementDTO>();

		// looping over all persons belonging to specified group
		for (PersonsEntity personE : personsEntityList) {
			// get personDTO corresponding to personEntity
			PersonDTO personDTO = peopleService
					.getPersonDTOByPersonEntity(personE);
			// get all measurements corresponding to personDTO
			MeasurementsDTO personMeasurementsDTO = peopleService
					.getAllMeasurementsByPerson(personDTO);
			// get list of measurements corresponding to specified measurement
			// type
			List<MeasurementDTO> listOfPersonMeasurement = getListOfMeasurementDTOFromMeasurementsByMeasurementname(
					measurementName, personMeasurementsDTO);
			// remove time part from date
			convertMeasurementDTOListdateTimeToDate(listOfPersonMeasurement);
			// order measurement list by date ascending
			orderByDateAssending(listOfPersonMeasurement);
			// substitute measurements made in one day with their average value
			listOfPersonMeasurement = calculateAverage(listOfPersonMeasurement);

			// add persons measurement list entries to list of all measurements
			for (MeasurementDTO measurementDTO : listOfPersonMeasurement) {
				listOfAllPersonsMeasurementDTOs.add(measurementDTO);
			}

		}

		// order by date ascending
		orderByDateAssending(listOfAllPersonsMeasurementDTOs);
		// calculate average per day
		listOfAllPersonsMeasurementDTOs = calculateAverage(listOfAllPersonsMeasurementDTOs);

		return listOfAllPersonsMeasurementDTOs;
	}

	/**
	 * 
	 * @param measurementName
	 *            - measurement type(e.g. "mood")
	 * @param measurementsDTO
	 *            - measurementsDTO containing all measurements for one person
	 * @return List of measurements containing measurement type specified by
	 *         measurementName param
	 */
	public List<MeasurementDTO> getListOfMeasurementDTOFromMeasurementsByMeasurementname(
			String measurementName, MeasurementsDTO measurementsDTO) {
		List<MeasurementDTO> listOfMeasurement = null;
		if (measurementName.equals("mood")) {
			listOfMeasurement = measurementsDTO.getMood();
		} else if (measurementName.equals("feeling")) {
			listOfMeasurement = measurementsDTO.getFeeling();
		} else if (measurementName.equals("weight")) {
			listOfMeasurement = measurementsDTO.getWeight();
		} else if (measurementName.equals("pulse")) {
			listOfMeasurement = measurementsDTO.getPulse();
		} else if (measurementName.equals("bloodOxygen")) {
			listOfMeasurement = measurementsDTO.getBloodOxygen();
		} else if (measurementName.equals("height")) {
			listOfMeasurement = measurementsDTO.getHeight();
		} else if (measurementName.equals("upperBloodPressure")) {
			listOfMeasurement = measurementsDTO.getUpperBloodPressure();
		} else if (measurementName.equals("lowerBloodPressure")) {
			listOfMeasurement = measurementsDTO.getLowerBloodPressure();
		} else if (measurementName.equals("bmi")) {
			listOfMeasurement = measurementsDTO.getBmi();
		}

		return listOfMeasurement;
	}

	/**
	 * 
	 * @param measurements
	 *            - list of MeasurementDTOs
	 * @return orderd list of MeasurementDTOs by date ascending
	 */
	public List<MeasurementDTO> orderByDateAssending(
			List<MeasurementDTO> measurements) {
		Collections.sort(measurements);

		return measurements;
	}

	/**
	 * 
	 * @param dateTime
	 *            - dateTame in millisecond, converted to String type
	 * @return same as input but Hour, Minute, Second and Time part of dateTime
	 *         set to zero
	 */
	public String convertDateTimeToDate(String dateTime) {

		long dateTimemilliSeconds = Long.parseLong(dateTime);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dateTimemilliSeconds);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR, 0);
		dateTimemilliSeconds = calendar.getTimeInMillis();

		return Long.toString(dateTimemilliSeconds);
	}

	/**
	 * 
	 * @param measurements
	 *            - list of MeasurementDTOs
	 * @return same list, but date property converted to not contain Hour,
	 *         Minute, Second and Time part
	 */
	public List<MeasurementDTO> convertMeasurementDTOListdateTimeToDate(
			List<MeasurementDTO> measurements) {

		for (MeasurementDTO measurement : measurements) {
			String date = measurement.getDate();
			date = this.convertDateTimeToDate(date);
			measurement.setDate(date);
		}

		return measurements;
	}

	/**
	 * 
	 * @param measurements
	 *            - list of MeasurementDTOs
	 * @return list of MeasurementDTOs containing average values per day
	 */
	public List<MeasurementDTO> calculateAverage(
			List<MeasurementDTO> measurements) {
		List<MeasurementDTO> averageMeasurements = new ArrayList<MeasurementDTO>();
		MeasurementDTO averageM = null;
		List<String> tempListOfValues = null;
		String runningDate = null;
		double sumOfVals = 0.0;
		double averageVal = 0.0;
		int counter = 0;

		for (MeasurementDTO mm : measurements) {
			if (runningDate == null) {
				runningDate = mm.getDate();
				tempListOfValues = new ArrayList<String>();
				tempListOfValues.add(mm.getValue());

			} else if (runningDate.equals(mm.getDate())) {
				tempListOfValues.add(mm.getValue());
			} else {
				sumOfVals = 0.0;
				counter = 0;
				for (String tempValue : tempListOfValues) {
					sumOfVals += Double.parseDouble(tempValue);
					counter++;
				}
				averageVal = sumOfVals / counter;
				averageM = new MeasurementDTO();
				averageM.setDate(runningDate);
				averageM.setValue(Double.toString(averageVal));
				averageMeasurements.add(averageM);
				runningDate = mm.getDate();
				tempListOfValues = new ArrayList<String>();
				tempListOfValues.add(mm.getValue());
			}

		}

		if (tempListOfValues != null && tempListOfValues.size() != 0) {
			//
			sumOfVals = 0.0;
			counter = 0;
			for (String tempValue : tempListOfValues) {
				sumOfVals += Double.parseDouble(tempValue);
				counter++;
			}
			averageVal = sumOfVals / counter;
			averageM = new MeasurementDTO();
			averageM.setDate(runningDate);
			averageM.setValue(Double.toString(averageVal));
			averageMeasurements.add(averageM);
		}

		return averageMeasurements;
	}

}
