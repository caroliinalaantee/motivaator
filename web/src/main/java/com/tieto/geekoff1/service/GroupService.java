package com.tieto.geekoff1.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.geekoff1.dao.interfaces.GroupDaoInterface;
import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.GroupDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Service("groupService")
@Transactional
public class GroupService implements GroupServiceInterface {

	@Autowired
	private GroupDaoInterface groupDao;
	@Autowired
	private PeopleDaoInterface peopleDao;
	@Autowired
	private PeopleServiceInterface peopleService;
	@Autowired
	private Logger logger;

	public GroupsEntity getGroupByGroupName(String name) {
		return groupDao.getGroupByGroupName(name);
	}

	public List<PersonsEntity> getAllGroupPersonsbyGroupName(String name) {
		return groupDao.getAllGroupPersonsbyGroupName(name);
	}

	public GroupsEntity createNewGroup(String label) {

		GroupsEntity group = new GroupsEntity();
		group.setGroupName(label);
		groupDao.saveNewGroup(group);

		return group;

	}

	public List<String> getAllGroupNames() {
		List<String> allGroupNames = groupDao.getAllGroupNames();
		return allGroupNames;
	}

	public GroupDTO getGroupDTOByName(String name) {
		GroupDTO group = new GroupDTO();
		group.setGroupName(name);

		GroupsEntity groupEntity = groupDao.getGroupByGroupName(name);

		Set<PersonsEntity> personEntitySet = new HashSet<PersonsEntity>(
				groupEntity.getPersonsList());
		List<PersonsEntity> personEntityList = new ArrayList<PersonsEntity>(
				personEntitySet);
		List<PersonWithMeasurementsDTO> personList = new ArrayList<PersonWithMeasurementsDTO>();
		for (PersonsEntity personEntity : personEntityList) {
			String firstName = personEntity.getFirstName();
			String lastName = personEntity.getLastName();
			PersonWithMeasurementsDTO person = peopleService
					.getPersonWithMeasurementsByName(firstName, lastName);
			personList.add(person);
		}

		group.setGroupMembers(personList);

		return group;
	}

	@Transactional
	public void deleteGroupFromPerson(String id, String name, String groupName) {
		if (id == null || name == null || groupName == null) {
			logger.debug("GroupService:deleteGroupFromPerson: personid: " + id
					+ ", nimi: " + name + " groupname: " + groupName);
		} else {
			PersonsEntity person = peopleDao.getPersonByIdAndName(id, name);
			GroupsEntity group = getGroupByGroupName(groupName);
			groupDao.deleteGroupFromPerson(person, group);
		}

	}

}
