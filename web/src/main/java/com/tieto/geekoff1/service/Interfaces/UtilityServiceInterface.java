package com.tieto.geekoff1.service.Interfaces;

import java.util.List;

import com.tieto.geekoff1.dto.MeasurementDTO;

/**
 * UtilityServiceInterface class contains method required to calculate
 * measurement average
 */
public interface UtilityServiceInterface {

	/**
	 * getAverageMeasurementListOverPeriod method calculates group average of
	 * one type measurement over periond
	 * 
	 * @param groupName
	 *            - group name
	 * @param measurementName
	 *            - measurement type (e.g. pulse)
	 * @return List of MeasurementDTO type
	 */
	public List<MeasurementDTO> getAverageMeasurementListOverPeriod(
			String groupName, String measurementName);

}
