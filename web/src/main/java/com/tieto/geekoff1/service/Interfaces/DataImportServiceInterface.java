package com.tieto.geekoff1.service.Interfaces;

import java.util.List;
import java.util.Map;

import com.tieto.geekoff1.domain.MeasurementsEntity;

/**
 * DataImportServiceInterface class contains required methods of data import
 */
public interface DataImportServiceInterface {

	/**
	 * readFromResourceAndTokenizeToMeasurements method reads data from
	 * measurement device and tokenizes measurements
	 * 
	 * @param resourceLocation
	 *            - dataUrl value in src/main/resources/paths.properties
	 * @return all measurement List of String type
	 */
	public abstract List<String> readFromResourceAndTokenizeToMeasurements(
			String resourceLocation);

	/**
	 * separateMeasurementSets method separates measurement sets by date
	 * 
	 * @param allMeasurements
	 *            - all measurement List of String type (return of
	 *            readFromResourceAndTokenizeToMeasurements method)
	 * @return List of date, contains List of one measurement set
	 */
	public abstract List<List<String>> separateMeasurementSets(
			List<String> allMeasurements);

	/**
	 * separateOneMeasurementSetToMeasurement method separates one measurement
	 * set
	 * 
	 * @param measurements
	 *            - List of one measurement set
	 * @return Map of Strings that contains key and value (e.g. weight,
	 *         weight_data)
	 */
	public abstract Map<String, String> separateOneMeasurementSetToMeasurement(
			List<String> measurements);

	/**
	 * createMeasurementsEntityFromData method creates a MeasurementsEntity from
	 * imported data
	 * 
	 * @param measurements
	 *            - Map of Strings that contains key and value (return of
	 *            separateOneMeasurementSetToMeasurement)
	 * @return MeasurementsEntity type object
	 */
	public abstract MeasurementsEntity createMeasurementsEntityFromData(
			Map<String, String> measurements);

	/**
	 * startDataImport method starts data importing, tokenizes measurements,
	 * separates measurement sets, creates MeasurementsEntity
	 * 
	 * @return boolean of successful data import
	 */
	public abstract boolean startDataImport();
}
