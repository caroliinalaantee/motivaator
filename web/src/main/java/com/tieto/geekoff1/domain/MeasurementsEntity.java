package com.tieto.geekoff1.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "MEASUREMENT")
public class MeasurementsEntity implements Comparable {

	@Id
	@Column(name = "M_ID")
	@GeneratedValue
	private long Id;

	@ManyToOne
	@JoinColumn(name = "PERSON_ID")
	@NotFound(action = NotFoundAction.IGNORE)
	private PersonsEntity insertPersonsEntity;

	/* mootmistulemused */
	@Column(name = "DATETIME")
	private String measureDate;

	@Column(name = "MOOD")
	private String mood; // enesetunne

	@Column(name = "FEEL")
	private String feel; // tuju

	@Column(name = "PULSE")
	private String pulse; // pulsi keskmine

	@Column(name = "SPO2")
	private String spo2; // hapnikusisaldus veres

	@Column(name = "SYS")
	private String sys; // �lemine verer�hk

	@Column(name = "DIA")
	private String dia; // alumine verer�hk

	@Column(name = "WEIGHT")
	private String weight; // kaal

	@Column(name = "HEIGHT")
	private String height; // pikkus

	@Column(name = "BMI")
	private String bmi; // kehamassi indeks

	@Column(name = "DEVICE_ID")
	private String deviceId;

	@Column(name = "PPG_PULSE")
	private String ppgPulse;

	public String getPpgPulse() {
		return ppgPulse;
	}

	public void setPpgPulse(String ppgPulse) {
		this.ppgPulse = ppgPulse;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public PersonsEntity getInsertPersonsEntity() {
		return insertPersonsEntity;
	}

	public void setInsertPersonsEntity(PersonsEntity insertPersonsEntity) {
		this.insertPersonsEntity = insertPersonsEntity;
	}

	public String getMeasureDate() {
		return measureDate;
	}

	public void setMeasureDate(String measureDate) {
		this.measureDate = measureDate;
	}

	public String getMood() {
		return mood;
	}

	public void setMood(String mood) {
		this.mood = mood;
	}

	public String getFeel() {
		return feel;
	}

	public void setFeel(String feel) {
		this.feel = feel;
	}

	public String getPulse() {
		return pulse;
	}

	public void setPulse(String pulse) {
		this.pulse = pulse;
	}

	public String getSpo2() {
		return spo2;
	}

	public void setSpo2(String spo2) {
		this.spo2 = spo2;
	}

	public String getSys() {
		return sys;
	}

	public void setSys(String sys) {
		this.sys = sys;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getBmi() {
		return bmi;
	}

	public void setBmi(String bmi) {
		this.bmi = bmi;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeasurementsEntity other = (MeasurementsEntity) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MeasurementsEntity [Id=" + Id + ", insertPersonsEntity="
				+ insertPersonsEntity + ", measureDate=" + measureDate
				+ ", mood=" + mood + ", feel=" + feel + ", pulse=" + pulse
				+ ", spo2=" + spo2 + ", sys=" + sys + ", dia=" + dia
				+ ", weight=" + weight + ", height=" + height + ", bmi=" + bmi
				+ ", deviceId=" + deviceId + ", ppgPulse=" + ppgPulse
				+ ", getPpgPulse()=" + getPpgPulse() + ", getId()=" + getId()
				+ ", getInsertPersonsEntity()=" + getInsertPersonsEntity()
				+ ", getMeasureDate()=" + getMeasureDate() + ", getMood()="
				+ getMood() + ", getFeel()=" + getFeel() + ", getPulse()="
				+ getPulse() + ", getSpo2()=" + getSpo2() + ", getSys()="
				+ getSys() + ", getDia()=" + getDia() + ", getWeight()="
				+ getWeight() + ", getHeight()=" + getHeight() + ", getBmi()="
				+ getBmi() + ", getDeviceId()=" + getDeviceId()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	public int compareTo(Object measurement) {
		return this.getMeasureDate().compareTo(
				(((MeasurementsEntity) measurement).getMeasureDate()));
	}

}
