package com.tieto.geekoff1.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

//k�igi gruppide nimed e labelid

@Entity
@Table(name = "GROUPS")
public class GroupsEntity implements Comparable {

	@Id
	@Column(name = "GR_ID")
	@GeneratedValue
	private long Id;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "PER_GR", joinColumns = @JoinColumn(name = "GRID", updatable = true), inverseJoinColumns = @JoinColumn(name = "PERID", updatable = true))
	@Fetch(FetchMode.SELECT)
	private List<PersonsEntity> personsList = new ArrayList<PersonsEntity>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupsEntity other = (GroupsEntity) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	@Column(name = "GROUP_NAME")
	private String groupName;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public List<PersonsEntity> getPersonsList() {
		return personsList;
	}

	public void setPersonsList(List<PersonsEntity> personsList) {
		this.personsList = personsList;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "GroupsEntity [Id=" + Id + ", personsList=" + personsList
				+ ", groupName=" + groupName + ", getId()=" + getId()
				+ ", getPersonsList()=" + getPersonsList()
				+ ", getGroupName()=" + getGroupName() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public int compareTo(Object group) {
		return -1
				* ((GroupsEntity) group).getGroupName().compareTo(
						this.getGroupName());
	}

}
