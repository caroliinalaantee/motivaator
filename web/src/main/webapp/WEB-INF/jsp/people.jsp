<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
	
	<body>
		<jsp:include page="navigation_menu.jsp"></jsp:include>
		
		<div id="people_list">
			<ul class="col-md-3">
				<c:forEach var="name" items="${namesList}">
					<a href="profile?name=${name}" id="listText" class="list-group-item">${name}</a>
		        </c:forEach>
			</ul>
		</div>
	</body>
</html>