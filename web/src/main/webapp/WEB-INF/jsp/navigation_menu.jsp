<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">

<script src="<c:url value="/jquery/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/js/main.js" />"></script>
<script src="<c:url value="/js/charts.js" />"></script>
<script src="<c:url value="/js/date.js" />"></script>
<link href="<c:url value="/css/main.css" />" rel="stylesheet">
<script src="<c:url value="/flotchart/jquery.flot.min.js" />"></script>
<script src="<c:url value="/flotchart/jquery.flot.time.js" />"></script>
<script src="<c:url value="/flotchart/jquery.flot.axislabels.js" />"></script>
<script src="<c:url value="/bootstrap/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/bootstrap/js/bootstrap-datepicker.js" />"></script>
<link href="<c:url value="/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/bootstrap/css/bootstrap-theme.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/bootstrap/css/datepicker.css" />"
	rel="stylesheet">
</head>

<body>

	<!-- Navigation bar -->
	
	<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="messages">Motivaator 1.0</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="people">Inimesed</a></li>
				<li><a href="groups">Grupid</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<form class="navbar-form navbar-left" role="search">
					<div id="searchGroupsAndPeople" class="navbar-form">
						<input class="form-control" id="searchInput"
							list="groupsAndPeople">
						<datalist id="groupsAndPeople"> <c:forEach var="obj"
							items="${searchList}">
							<option value="${obj}">
						</c:forEach>
						<option value="test">
						</datalist>

						<button type="button" class="btn btn-primary"
							onclick="openProfile('${namesList}', '${allGroupsList}')">Mine</button>
					</div>
				</form>
			</ul>
		</div>
	</div>
	</nav>

</body>
</html>