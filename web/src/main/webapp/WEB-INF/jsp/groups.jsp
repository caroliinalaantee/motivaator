<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Grupid</title>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>

<body>
	<jsp:include page="navigation_menu.jsp"></jsp:include>

	<!-- List of groups -->
	<div id="groups_list">
		<ul class="col-md-3">
			<c:forEach var="groupName" items="${groupsList}">
				<a id="listText" class="list-group-item"
					href="group?name=${groupName}">${groupName}</a>
			</c:forEach>
		</ul>
	</div>
</body>
</html>