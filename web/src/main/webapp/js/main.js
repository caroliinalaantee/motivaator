/*
 * On load, create and initiate datepickers
 */
$(document).ready(function() {
	$('#startTime').datepicker({
		format : "dd/mm/yyyy",
		autoclose : true,
		weekStart : 1
	});
	$('#endTime').datepicker({
		format : "dd/mm/yyyy",
		autoclose : true,
		weekStart : 1
	});
	
	/*
	 * Hide datepicker when date has been chosen
	 */
	$('.day').on('click', function () {
	    $('.datepicker.dropdown-menu').hide();
	});
	
	$('#startTime').val(get30DaysAgo());
	$('#endTime').val(getNow());
	//$('#updateGraphs').click();

	$('#addLabelButton').click(function() {
		console.log($('#labelInput').val());
	});
});

/*
 * Add the group to the persons profile after submission
 */
function addGroupToPerson() {
	var labelString = $('#groupInput').val();
	var idNumber = $('#idNumber').text();
	var firstName = $('#personName').text().split(" ")[0];
	idNumber = idNumber.substring(idNumber.indexOf(":") + 2)
	var hrefString = "/web/addlabel?label=" + labelString + "&id=" + idNumber
			+ "&firstName=" + firstName;
	if(labelString != "") {
		window.location.href = hrefString;
	} else {
		$('#alertGroup').css( "display", "" );
	}
	
}

/*
 * Add the person to the group profile after submission
 */
function addPersonToGroup() {
	var labelString = $('#personInput').val();
	var groupName = $('#groupProfileName').text();
	var hrefString = "/web/addpersonlabel?label=" + labelString + "&groupName=" + groupName;
	if(labelString != "") {
		window.location.href = hrefString;
	} else {
	}
	
}

/*
 * Save new comment
 */
function saveComment() {
	var commentString = $('#add-comment').val();
	var idNumber = $('#idNumber').text();
	var firstName = $('#personName').text().split(" ")[0];
	idNumber = idNumber.substring(idNumber.indexOf(":") + 2)
	var hrefString = "/web/savecomment?comment=" + commentString + "&id=" + idNumber
			+ "&firstName=" + firstName;
	
	window.location.href = hrefString;
}

/*
 * Save new family name given to the user
 */
function savePersonName() {
	var familyName = $('#nameInput').val();
	var idNumber = $('#idNumber').text();
	var firstName = $('#personName').text().split(" ")[0];
	idNumber = idNumber.substring(idNumber.indexOf(":") + 2)
	var hrefString = "/web/changeName?lastName=" + familyName + "&id=" + idNumber
			+ "&firstName=" + firstName;
	console.log("index of space: " + familyName.indexOf(' '));
	
	if((familyName != "") && (familyName.indexOf(' ') < 0)) {
		window.location.href = hrefString;
	} else {
		$('#alertName').css( "display", "" );
	}
	
}

/*
 * Delete user from a group
 */
function deleteFromGroup(groupName) {
	var idNumber = $('#idNumber').text();
	var firstName = $('#personName').text().split(" ")[0];
	idNumber = idNumber.substring(idNumber.indexOf(":") + 2)
	var hrefString = "/web/deleteGroup?groupName=" + groupName + "&id=" + idNumber
			+ "&firstName=" + firstName;
	window.location.href = hrefString;
}

/*
 * Open profile after choosing person/group when using form on the navigation
 * menu
 */
function openProfile(names, groups) {

	names = names.replace("[", "");
	names = names.replace("]", "");
	var nameList = names.split(",");

	groups = groups.replace("[", "");
	groups = groups.replace("]", "");
	var groupList = groups.split(",");

	for (i = 0; i < nameList.length; i++) {
		nameList[i] = nameList[i].trim();
	}

	for (i = 0; i < groupList.length; i++) {
		groupList[i].trim();
	}

	var searchString = $('#searchInput').val();
	
	console.log($.inArray(searchString, nameList));
	console.log(groupList);
	
	if ($.inArray(searchString, nameList) >= 0) {
		var hrefString = "/web/profile?name=" + searchString;
		window.location.href = hrefString;
	} else {
		var hrefString = "/web/group?name=" + searchString;
		window.location.href = hrefString;
	}
	

}
