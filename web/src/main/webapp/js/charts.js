/*
 * Call drawing functions with specific data for each table
 */
function drawTables(moodData, feelingData, weightData, pulseData,
		bloodOxygenData, bmiData, ubp, lbp, personName, groupMood, groupFeeling) {
	drawTable(weightData, "#weight", 10);
	drawTable(pulseData, "#pulse", 10);
	drawTable(bloodOxygenData, "#bloodOxygen", 10);
	drawTable(bmiData, "#bmi", 1);
	
	console.log(groupMood);

	if (groupMood !== "[]"){
		drawPersonVsGroupTable(personName, moodData, groupMood, "#feeling", "#feelingchoices");
		drawPersonVsGroupTable(personName, feelingData, groupFeeling, "#mood", "#moodchoices");
	} else {
		drawTable(moodData, "#mood", 1);
		drawTable(feelingData, "#feeling");
	}
	drawBloodPressure(ubp, lbp);
}

var globaalne_massiiv = [];
var globaalne_number = 0;
/*
 * Draw bloodpressure table with specific options and data
 */
function drawBloodPressure(ubp, lbp) {
	var time = getStartAndEnd();

	var data1 = getDataFromString(ubp);
	var data2 = getDataFromString(lbp);

	var ms = time.end - time.start;

	var d, h, m, s;
	s = Math.floor(ms / 1000);
	m = Math.floor(s / 60);
	s = s % 60;
	h = Math.floor(m / 60);
	m = m % 60;
	d = Math.floor(h / 24);
	h = h % 24;
	var timeTick = Number(d / 10);

	var options = {
		xaxes : [ {
			min : time.start,
			max : (time.end) + 86400000,
			mode : "time",
			timeformat : "%d/%m/%y",
			tickSize : [ timeTick, "day" ]
		} ],
		yaxis : {
			min : 50,
			max : 200,
			tickSize : 20
		},
		legend : {
			backgroundOpacity : 0.2,
			noColumns : 2,
			backgroundColor : "gray",
			position : "nw"
		},
		series : {
			lines : {
				show : true
			},
			points : {
				show : true
			}
		},
		grid : {
			hoverable : true,
			markings : weekendAreas
		}
	};

	var dataset = [ {
		data : data1,
		label : " &Uuml;lemine "
	}, {
		data : data2,
		label : " Alumine "
	} ];

	$.plot($("#bloodPressure"), dataset, options);
}

/*
 * Create options for tables
 */
function makeOptions(time, data, type, tick) {

	var type = type.substring(1);
	var minValue;
	var maxValue;
	var range = maxValue - minValue;

	if (type === "mood") {
		minValue = 0;
		maxValue = 5;
	} else if (type === "feeling") {
		minValue = 0;
		maxValue = 5;
	} else if (type === "weight") {
		minValue = 50;
		maxValue = 150;
	} else if (type === "pulse") {
		minValue = 50;
		maxValue = 200;
	} else if (type === "bmi") {
		minValue = 10;
		maxValue = 30;
	} else if (type === "bloodOxygen") {
		minValue = 50;
		maxValue = 100;
	}

	var ms = time.end - time.start;
	var d, h, m, s;
	s = Math.floor(ms / 1000);
	m = Math.floor(s / 60);
	s = s % 60;
	h = Math.floor(m / 60);
	m = m % 60;
	d = Math.floor(h / 24);
	h = h % 24;
	var timeTick = Number(d / 10);

	return {
		xaxes : [ {
			min : time.start,
			max : (time.end) + 86400000,
			mode : "time",
			timeformat : "%d/%m/%y",
			tickSize : [ timeTick, "day" ]
		} ],
		yaxis : {
			min : minValue,
			max : maxValue,
			tickSize : tick
		},
		series : {
			lines : {
				show : true
			},
			points : {
				show : true
			}
		},
		grid : {
			hoverable : true,
			markings : weekendAreas
		}
	};
}

/*
 * Get startTime and endTime from datepickers
 */
function getStartAndEnd() {
	var startTimeString = $('#startTime').val();
	var endTimeString = $('#endTime').val();
	var start = startTimeString.split("/");
	var end = endTimeString.split("/");
	var startDate = new Date(Number(start[2]), Number(start[1]) - 1,
			Number(start[0]));
	var endDate = new Date(Number(end[2]), Number(end[1]) - 1, Number(end[0]));
	if (startDate.getTime() > endDate.getTime()) {
		alert("Kuupäevad on valepidi sisestatud!");
	}

	return {
		start : startDate.getTime(),
		end : endDate.getTime()
	}
}

/*
 * Get today's date
 */
function getNow() {
	var now = new Date();
	return now.toString("dd/MM/yyyy");
}

/*
 * Get a date 30 days ago from today
 */
function get30DaysAgo() {
	var now = new Date().getTime();
	var then = new Date(now - 2592000000);
	return then.toString("dd/MM/yyyy");
}

/*
 * Parse data
 */
function getDataFromString(string) {
	var results = stringToArray(string);

	var data = [];
	var temp = [];

	while (results.length > 0) {
		temp.push(Number(results.splice(0, 1)));
		temp.push(Number(results.splice(0, 1)));

		data.push(temp);

		temp = [];
	}
	return data;
}

/*
 * Parse data
 */
function stringToArray(string) {
	var res1 = string.replace("[", "");
	var res2 = res1.replace("]", "");
	var res3 = res2.replace(/,/g, " ");
	var results = res3.replace(/\s{2,}/g, " ");
	results = results.split(" ");
	return results;
}

/*
 * Draw table using measurement data, placeholder as location, range, tick
 */
function drawTable(measurementData, placeholder, minVal, maxVal, tick) {

	var time = getStartAndEnd();
	var data = getDataFromString(measurementData);

	var options = makeOptions(time, data, placeholder, tick);

	$.plot($(placeholder), [ data ], options);
}

/*
 * Return weekend areas to show on tables
 */
function weekendAreas(axes) {

	var markings = [], d = new Date(axes.xaxis.min);

	/*
	 * Go to the first Saturday
	 */
	d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
	d.setUTCSeconds(0);
	d.setUTCMinutes(0);
	d.setUTCHours(0);

	var i = d.getTime();

	do {
		markings.push({
			xaxis : {
				from : i,
				to : i + 2 * 24 * 60 * 60 * 1000
			}
		});
		i += 7 * 24 * 60 * 60 * 1000;
	} while (i < axes.xaxis.max);

	return markings;
}

/*
 * Create data sets for tables with several datalines
 */
function makeDatasets(personData, groupsDataArray) {
	personData = getDataFromString(personData);
	var datasets = {
		"personData" : {
			label : "personData",
			data : personData
		}
	}
	for (var i = 0; i < groupsDataArray.length; i++) {
		var obj = groupsDataArray[i];
		datasets[obj.name] = {
			label : obj.name,
			data : splitToNumberArray(obj.data)
		}
	}
	return datasets;
}

function splitToNumberArray(string) {
	var string1 = String(string);
	var string2 = string1.replace("[", "");
	var string3 = string2.replace("]", "");
	var results1 = string3.split(",");

	var data = [];
	var temp = [];
	while (results1.length > 0) {
		temp.push(Number(results1.splice(0, 1)));
		temp.push(Number(results1.splice(0, 1)));
		data.push(temp);
		temp = [];
	}
	data.splice(data.length - 1);
	return data;
}
	
function parseGroupData(data) {
	// vahetan | jutumärkideks ja võtan viimase koma ära
	data = data.replace(/\|/g, '"');
	data = data.substring(0, data.length - 2);
	if (data !== "[]") {
		data += "]";
	}
	return JSON.parse(data);
}

function makeDataset(personName, personData, groupData) {
	var groupData = parseGroupData(groupData);
	//console.log("\nmake dataset() \nname: " + personName + "\nperson data: "
	//		+ personData + " \ngroup data: " + JSON.stringify(groupData));
	var datasets = {
		personName : {
			label : personName,
			data : personData
		}
	}
	var i;
	for (i = 0; i < groupData.length; i += 1) {
		groupData[i].data = splitToNumberArray(groupData[i].data);
		datasets[groupData[i].name] = {
			label : groupData[i].name,
			data : groupData[i].data
		}
	}
	return datasets;
}

function drawPersonVsGroupTable(personName, personData, groupData, placeholder, choices) {
	console.log("person name: " + personName + "\nperson data: " + personData + "group data: " + groupData + "placeholder: " + placeholder + "choices: " + choices);
	if (typeof groupData !== "undefined"){
		var personData = getDataFromString(personData);
		var time = getStartAndEnd();
		var dataset = makeDataset(personName, personData, groupData);
		drawMoodTable(dataset, placeholder, choices);
	} else {
		drawTable(personData, "#mood", 1);
	}
}

function addColors(datasets){
	$.each(datasets, function(key, value) {
		value.color = globaalne_number;
		globaalne_number += 1
	});
}


function drawMoodTable (datasets, placeholder, choices){
	var time = getStartAndEnd();
	var options = makeOptions(time, datasets, "mood", 1);
	addColors(datasets);
	var choiceContainer = $(choices);

	$.each(datasets, function(key, val) {
		while (choiceContainer.firstChild) {
			choiceContainer.removeChild(choiceContainer.firstChild);
		}
		choiceContainer.append("<br/><input class='checkbox' type='checkbox' name='" + key
				+ "' checked='checked' id='id" + key + "'></input>"
				+ "<label for='id" + key + "'>" + val.label + "</label>");
	});

	choiceContainer.find("input").click( plotGraph );
	
	function plotGraph() {
		var data = [];
		console.log(JSON.stringify(datasets));
		var time = getStartAndEnd();
		var options = makeOptions(time, datasets, "mood", 1);
		
		
		choiceContainer.find("input:checked").each(function () {
			var key = $(this).attr("name");
			data.push(datasets[key]);
		});

		globaalne_massiiv.push($.plot(placeholder, data, options));
	}
	plotGraph.call();
}

function redraw(){
	console.log("redraw");
	
	var cbarray = document.getElementsByClassName('checkbox');
    for(var i = 0; i < cbarray.length; i++){

        cbarray[i].checked = false;
        
    }

	var time = getStartAndEnd();
	var i;
	for (i = 0; i < globaalne_massiiv.length; i += 1) {
		console.log(globaalne_massiiv[i].getAxes().xaxis.options);
		globaalne_massiiv[i].getAxes().xaxis.options.min = time.start;
		globaalne_massiiv[i].getAxes().xaxis.options.max = time.end;
		globaalne_massiiv[i].setupGrid();
		globaalne_massiiv[i].draw();
	}
}

function plotGraph(choiceContainer, datasets) {
	var data = [];
	choiceContainer.find("input:checked").each(function () {
		var key = $(this).attr("name");
		data.push(datasets[key]);
	});
	$.plot("#moodTableGroup", data, options);
	
}