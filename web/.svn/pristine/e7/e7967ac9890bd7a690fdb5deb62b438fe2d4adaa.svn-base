package com.tieto.geekoff1.helloservices;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.DPSInterface;


public class DummyPersonsService {
	
	static Random random = new Random();
	static List<PersonWithMeasurementsDTO> allPersonsWithMeasurements = new ArrayList<PersonWithMeasurementsDTO>();
	static int numberOfDays = 30;
	
	public static List<PersonWithMeasurementsDTO> getAllPersonsWithMeasurements() {
		return allPersonsWithMeasurements;
	}

	public static PersonWithMeasurementsDTO getPersonByFirstnameAndLastname(String firstName, String lastName) {
		PersonWithMeasurementsDTO person = null;
		for (PersonWithMeasurementsDTO p : allPersonsWithMeasurements) {
			if (p.getPerson().getFirstName().equals(firstName)
					&& p.getPerson().getLastName().equals(lastName)){
				person = p;
			}
		}
		return person;
	}
	
	static {
		// vello
		PersonDTO person1 = new PersonDTO();

		person1.setFirstName("Vello");
		person1.setLastName("Viiuldaja");
		person1.setIdNumber("588111222");
		
		PersonWithMeasurementsDTO personWithMeaMesurement1 = new PersonWithMeasurementsDTO();
		personWithMeaMesurement1.setPerson(person1);
		personWithMeaMesurement1.setMeasurements(makeRandomMeasurements());
		
		allPersonsWithMeasurements.add(personWithMeaMesurement1);

		// mari
		PersonDTO person2 = new PersonDTO();

		person2.setFirstName("Mari");
		person2.setLastName("Maasikas");
		person2.setIdNumber("381234567");
	
		PersonWithMeasurementsDTO personWithMeaMesurement2 = new PersonWithMeasurementsDTO();
		personWithMeaMesurement2.setPerson(person2);
		personWithMeaMesurement2.setMeasurements(makeRandomMeasurements());
		
		allPersonsWithMeasurements.add(personWithMeaMesurement2);
	
		// reet
		PersonDTO person3 = new PersonDTO();

		person3.setFirstName("Reet");
		person3.setLastName("Redis");
		person3.setIdNumber("470707070");
		
		PersonWithMeasurementsDTO personWithMeaMesurement3= new PersonWithMeasurementsDTO();
		personWithMeaMesurement3.setPerson(person3);
		personWithMeaMesurement3.setMeasurements(makeRandomMeasurements());
		
		allPersonsWithMeasurements.add(personWithMeaMesurement3);
	
		// Jaan
		PersonDTO person4 = new PersonDTO();

		person4.setFirstName("Jaan");
		person4.setLastName("Jalgratas");
		person4.setIdNumber("373737373");
		
		PersonWithMeasurementsDTO personWithMeaMesurement4= new PersonWithMeasurementsDTO();
		personWithMeaMesurement4.setPerson(person4);
		personWithMeaMesurement4.setMeasurements(makeRandomMeasurements());
		
		allPersonsWithMeasurements.add(personWithMeaMesurement4);
	}
	
	private static MeasurementsDTO makeRandomMeasurements() {
		List<MeasurementDTO> mood = makeRandomMoodList();
		List<MeasurementDTO> feeling = makeRandomFeelingList();
		List<MeasurementDTO> weight = makeRandomWeights();
		List<MeasurementDTO> pulse = makeRandomPulse();
		List<MeasurementDTO> bloodOxygen = makeBloodOxygen();
		
		MeasurementsDTO measurements = new MeasurementsDTO();
		measurements.setFeeling(feeling);
		measurements.setMood(mood);
		measurements.setWeight(weight);
		measurements.setBloodOxygen(bloodOxygen);
		measurements.setPulse(pulse);

		return measurements;
	}

	private static List<MeasurementDTO> makeRandomPulse() {
		List<MeasurementDTO> pulseList =  new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO pulse = new MeasurementDTO();
			pulse.setValue(80 - random.nextInt(10));
			pulse.setDate(new Date(2014, 8, i + 1));
			pulse.setUnit("bmp");
			pulseList.add(pulse);
		}
		return pulseList;
	}

	private static List<MeasurementDTO> makeBloodOxygen() {
		List<MeasurementDTO> moodList =  new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO mood = new MeasurementDTO();
			mood.setValue(100 - random.nextInt(10));
			mood.setDate(new Date(2014, 8, i + 1));
			mood.setUnit("kg");
			moodList.add(mood);
		}
		return moodList;
	}

	private static List<MeasurementDTO> makeRandomWeights() {
		List<MeasurementDTO> moodList =  new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO mood = new MeasurementDTO();
			mood.setValue(random.nextInt(10) + 80);
			mood.setDate(new Date(2014, 8, i + 1));
			mood.setUnit("kg");
			moodList.add(mood);
		}
		return moodList;
	}

	private static List<MeasurementDTO> makeRandomMoodList() {
		List<MeasurementDTO> moodList =  new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO mood = new MeasurementDTO();
			mood.setValue(random.nextInt(10) + 1);
			mood.setDate(new Date(2014, 8, i + 1));
			mood.setUnit("percent");
			moodList.add(mood);
		}
		return moodList;
	}
	
	private static List<MeasurementDTO> makeRandomFeelingList() {
		List<MeasurementDTO> feelingList = new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO feeling = new MeasurementDTO();
			feeling.setValue(random.nextInt(10) + 1);
			feeling.setDate(new Date(2014, 8, i + 1));
			feeling.setUnit("percent");
			feelingList.add(feeling);
		}
		return feelingList;
	}
}
