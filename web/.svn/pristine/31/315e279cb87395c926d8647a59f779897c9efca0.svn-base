package com.tieto.geekoff1.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.GroupServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Controller
public class ProfileController {
	
	@Autowired
	@Qualifier("peopleControllerService")
	private PeopleServiceInterface peopleService;

	@Autowired
	private GroupServiceInterface groupService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String getRecords(@RequestParam(value="name", required=false) String personName, Model model) {
		model.addAttribute("namesList", getNameList());

		PersonWithMeasurementsDTO personWithMeasurement = getPersonWithMeasurements(personName);
		PersonDTO person = personWithMeasurement.getPerson();

        addPersonalInfoToProfile(model, person);
        addMeasurementsToProfile(model, personWithMeasurement);
        addGroupListToProfile(model, person);
        addCommentsToProfile(model, personWithMeasurement);

        addAllGroupNames(model);

        addSearchList(model);
		return "profile";
    }
	
	private void addCommentsToProfile(Model model, PersonWithMeasurementsDTO personWithMeasurements){
		String comment = personWithMeasurements.getComment();
		//TODO kommentaar on puudu, vaja �ra vahetada
		model.addAttribute("comment", "kommentaar");
	}
	
	private void addSearchList(Model model) {
		List<String> searchList = groupService.getAllGroupNames();
		searchList.addAll(getNameList());
		model.addAttribute("searchList", searchList);
	}

	private void addAllGroupNames(Model model) {
		model.addAttribute("allGroupsList", groupService.getAllGroupNames());
	}
	
	private void addGroupListToProfile(Model model, PersonDTO person) {
		model.addAttribute("groupsList", person.getGroupList()); 
	}
	
	private PersonWithMeasurementsDTO getPersonWithMeasurements(String name) {
		String firstName = name.split(" ")[0];
		String lastName = name.split(" ")[1];
		return peopleService.getPersonWithMeasurementsByName(firstName, lastName);
	}

    private void addMeasurementsToProfile(Model model, PersonWithMeasurementsDTO personWithMesurement) {
        model.addAttribute("mood", getMoodList(personWithMesurement));
        model.addAttribute("feeling", getFeelingList(personWithMesurement));
        model.addAttribute("weight", getWeightList(personWithMesurement));
        model.addAttribute("pulse", getPulseList(personWithMesurement));
        model.addAttribute("bloodOxygen", getBloodOxygenList(personWithMesurement));
        model.addAttribute("height", getHeightList(personWithMesurement));
        model.addAttribute("upperBloodPressure", getUpperBloodPressure(personWithMesurement));
        model.addAttribute("lowerBloodPressure", getLowerBloodPressure(personWithMesurement));
        model.addAttribute("bmi", getBmiList(personWithMesurement));
        
        for (String groupName : personWithMesurement.getPerson().getGroupList()){
        	List<MeasurementDTO> measurements = groupService.getAverageMeasurementListOverPeriod(groupName, "mood");
        	//TODO kui funktsioon service'is valmis saab, siis saab k�ima panna siit
        	//model.addAttribute(groupName + "mood", getMeasurementStringList(measurements));
        }
    }
    
    private List<String> getMeasurementStringList(List<MeasurementDTO> measurements) {
    	List<String> list = new ArrayList<String>();
    	for (MeasurementDTO measurement : measurements) {
    		list.add(measurement.toString());
    	}
    	return list;
    }
    
	private void addPersonalInfoToProfile(Model model, PersonDTO person) {
		String firstName = person.getFirstName();
        String lastName = person.getLastName();
        String idNumber = person.getIdNumber();
        String birthday = getBirthdayFromIdNumber(idNumber);
        String age = getAgeFromBirthday(birthday);
        
	    model.addAttribute("firstName", firstName);
        model.addAttribute("lastName", lastName);
        model.addAttribute("birthday", birthday);
        model.addAttribute("idNumber", idNumber);
        model.addAttribute("age", age);
	}

	private List<String> getBmiList(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getBmi()) {
			list.add(measurement.toString());
		}
		return list;
	}
			
	private List<String> getLowerBloodPressure(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getLowerBloodPressure()) {
			list.add(measurement.toString());
		}
		return list;
	}
	
	private List<String> getUpperBloodPressure(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getUpperBloodPressure()) {
			list.add(measurement.toString());
		}
		return list;
	}
	
	private List<String> getHeightList(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getHeight()) {
			list.add(measurement.toString());
		}
		return list;
	}
		
	private List<String> getBloodOxygenList(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getBloodOxygen()) {
			list.add(measurement.toString());
		}
		return list;
	}

	private List<String> getPulseList(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getPulse()) {
			list.add(measurement.toString());
		}
		return list;
	}

	public List<String> getNameList() {
		List<PersonDTO> people = peopleService.getAllPersons();
		List<String> names = new ArrayList<String>();
		for (PersonDTO p : people) {
			names.add(getFullname(p));
		}
		return names;
	}
	
	public String getFullname(PersonDTO person) {
		return person.getFirstName() + " " + person.getLastName();
	}
	
	private List<String> getWeightList(PersonWithMeasurementsDTO person) {
		List<String> list = new ArrayList<String>();
		for (MeasurementDTO measurement : person.getMeasurements().getWeight()) {
			list.add(measurement.toString());
		}
		return list;
	}

	private List<String> getFeelingList(
			PersonWithMeasurementsDTO personWithMesurement) {
		List<MeasurementDTO> feelingList = personWithMesurement.getMeasurements().getFeeling();
		List<String> lst = new ArrayList<String>();
		for (MeasurementDTO m : feelingList) {
			lst.add(m.toString());
		}
		return lst;
	}

	private List<String> getMoodList(
			PersonWithMeasurementsDTO personWithMesurement) {
		List<MeasurementDTO> moodList = personWithMesurement.getMeasurements().getMood();
		List<String> lst = new ArrayList<String>();
		for (MeasurementDTO m : moodList) {
			lst.add(m.toString());
		}
		return lst;
	}

	private String getAgeFromBirthday(String birthday) {
		return String.valueOf(2014 - Integer.parseInt(birthday.substring(6)));
	}

	private String getBirthdayFromIdNumber(String idNumber) {
        return idNumber.substring(5, 7) + "." + idNumber.substring(3, 5) + ".19" + idNumber.substring(1, 3);
	}
}
