package com.tieto.geekoff1.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

@Transactional
@Component
public class PeopleDAO implements PeopleDaoInterface {

	@Autowired
	private SessionFactory sessionFactory;

	public PersonsEntity getPersonByNameAndFamilyName(String name, String lastName){
		System.out.println("hello from DAO. Activated getPersonByNameAndFamilyName name to search is "+name +" "+ lastName);
		
		PersonsEntity personEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			personEntity = (PersonsEntity) session.createCriteria(PersonsEntity.class)
								.add(Restrictions.eq("firstName", name))
								.add(Restrictions.eq("lastName", lastName))
								.uniqueResult();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return personEntity;
		
	}

	public PersonsEntity getPersonByIdAndName(String id, String name) {
		System.out.println("hello from DAO. Activated getPersonByIdAndName search parameters are "+id +" "+ name);
		
		PersonsEntity personEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			personEntity = (PersonsEntity) session.createCriteria(PersonsEntity.class)
								.add(Restrictions.eq("personId", id))
								.add(Restrictions.eq("firstName", name))
								.uniqueResult();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return personEntity;
	}
	
	@SuppressWarnings("unchecked")
	public List<PersonsEntity> getAllPersons() {
		System.out.println("hello from DAO, activating getAllPersons, getting all persons List");
		
		Set<PersonsEntity> personsEntitySet = new HashSet<PersonsEntity>();
		List<PersonsEntity> personsEntityList=new ArrayList<PersonsEntity>(personsEntitySet);
		
		Session session = sessionFactory.getCurrentSession();
		try {
			personsEntitySet = new HashSet<PersonsEntity>( session.createCriteria(PersonsEntity.class)
							.list());
			personsEntityList=new ArrayList<PersonsEntity>(personsEntitySet);
			Collections.sort(personsEntityList);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return personsEntityList;
	}

	public List<MeasurementsEntity> getPersonMeasurementsByPersonName(
			String name, String lastName) {
		System.out.println("hello from DAO. Activated getPersonMeasurementsByPersonName name to search is "+name +" "+ lastName);
		
		PersonsEntity person = getPersonByNameAndFamilyName(name, lastName);
		Set<MeasurementsEntity> personMeasurementsSet = new HashSet<MeasurementsEntity>();
		if (person == null){
			return null;
		}
		personMeasurementsSet = person.getMeasurementsList();
		List<MeasurementsEntity> personMeasurementsList=new ArrayList<MeasurementsEntity>(personMeasurementsSet);
		Collections.sort(personMeasurementsList);
		return personMeasurementsList;
	}

	public void saveNewPerson(PersonsEntity person) {
		System.out.println("hello from DAO. Activated saveNewPerson. Saving person to the database");
		Session session = sessionFactory.getCurrentSession();
			session.save(person);
	}

	public void saveMeasurementToExistingPerson(PersonsEntity person,
			MeasurementsEntity measurement) {
		System.out.println("hello from DAO. Activated saveMeasurementToExistingPerson. Saving measurements to the database");
		Session session = sessionFactory.getCurrentSession();
		
		person.getMeasurementsList().add(measurement);
		measurement.setInsertPersonsEntity(person);
		session.save(measurement);
		session.update(person);
		
	}

	public void saveExistingGroupToPerson(PersonsEntity person,
			GroupsEntity group) {
		System.out.println("hello from DAO. Activated saveExistingGroupToPerson. Adding group to the person");
		Session session = sessionFactory.getCurrentSession();

		if (person == null) {
			System.out.println("person on null");
		}
		if (group == null) {
			System.out.println("group on null");
		}

		person.getGroups().add(group);
		group.getPersonsList().add(person);
		
		session.update(group);
		session.update(person);
	}

	public MeasurementsEntity getMeasurementsEntityByDateTime(String dateTime) {
		MeasurementsEntity MeasurementsEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			MeasurementsEntity = (MeasurementsEntity) session.createCriteria(MeasurementsEntity.class)
								.add(Restrictions.eq("measureDate", dateTime))
								.uniqueResult();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return MeasurementsEntity;
	}

	public void saveOrUpdateCommentToPerson(String id, String name, String comment) {
		PersonsEntity person = getPersonByIdAndName(id, name);
		person.setComment(comment);
		
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(person);
		
	}

	public void saveOrUpdateLastName(String id, String name, String lastName) {
		PersonsEntity person = getPersonByIdAndName(id, name);
		person.setLastName(lastName);
		Session session = sessionFactory.getCurrentSession();
		session.update(person);
		
	}
	
}
