package com.tieto.geekoff1.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.dto.GroupDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.GroupControllerServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleControllerServiceInterface;

@Controller
public class GroupController {
	
	@Autowired
	private GroupControllerServiceInterface groupService;
	
	@Autowired
	private PeopleControllerServiceInterface peopleService;

	@RequestMapping(value = "/group", method = RequestMethod.GET)
    public String getRecords(@RequestParam(value="name", required=false) String groupName, Model model) {
		addGroupNameList(model);
		addGroupInfo(model, groupName);
		model.addAttribute("groupName", groupName);
		
		addSearchList(model);
        return "group";
    }
	
	private void addSearchList(Model model) {
		List<String> searchList = groupService.getAllGroupNames();
		searchList.addAll(getNameList());
		model.addAttribute("searchList", searchList);
	}
	
	public List<String> getNameList() {
		List<PersonDTO> people = peopleService.getAllPersons();
		List<String> names = new ArrayList<String>();
		for (PersonDTO p : people) {
			names.add(getFullname(p));
		}
		return names;
	}
	
	public String getFullname(PersonDTO person) {
		return person.getFirstName() + " " + person.getLastName();
	}
	
	private void addGroupInfo(Model model, String groupName) {
		GroupDTO group = groupService.getGroupDTOByName(groupName);
		List<String> groupMemberStringList = new ArrayList<String>();
		for (PersonWithMeasurementsDTO person : group.getGroupMembers()){
			String firstName = person.getPerson().getFirstName();
			String lastName = person.getPerson().getLastName();
			groupMemberStringList.add(firstName + " " + lastName);
		}
		model.addAttribute("peopleList", groupMemberStringList);
	}

	private void addGroupNameList(Model model) {
		model.addAttribute("groupsList", groupService.getAllGroupNames());
	}
}

