/*
 * Call drawing functions with specific data for each table
 */
function drawTables(moodData, feelingData, weightData, pulseData,
		bloodOxygenData, bmiData, ubp, lbp) {
	drawTable(moodData, "#mood", 1);
	drawTable(feelingData, "#feeling", 1);
	drawTable(weightData, "#weight", 10);
	drawTable(pulseData, "#pulse", 10);
	drawTable(bloodOxygenData, "#bloodOxygen", 10);
	drawTable(bmiData, "#bmi", 1);

	drawBloodPressure(ubp, lbp);
}

/*
 * Draw bloodpressure table with specific options and data
 */
function drawBloodPressure(ubp, lbp) {
	var time = getStartAndEnd();

	var data1 = getDataFromString(ubp);
	var data2 = getDataFromString(lbp);

	var milliseconds = time.end - time.start;

	var d, h, m, s;
	s = Math.floor(milliseconds / 1000);
	m = Math.floor(s / 60);
	s = s % 60;
	h = Math.floor(m / 60);
	m = m % 60;
	d = Math.floor(h / 24);
	h = h % 24;
	var timeTick = Number(d / 10);

	var options = {
		xaxes : [ {
			min : time.start,
			max : time.end,
			mode : "time",
			timeformat : "%d/%m/%y",
			tickSize : [ timeTick, "day" ]
		} ],
		yaxis : {
			min : 50,
			max : 200,
			tickSize : 20
		},
		legend : {
			backgroundOpacity : 0.2,
			noColumns : 2,
			backgroundColor : "gray",
			position : "nw"
		},
		series : {
			lines : {
				show : true
			},
			points : {
				show : true
			}
		},
		grid : {
			hoverable : true,
			markings : weekendAreas
		}
	};

	var dataset = [ {
		data : data1,
		label : " &Uuml;lemine "
	}, {
		data : data2,
		label : " Alumine "
	} ];

	$.plot($("#bloodPressure"), dataset, options);
}

/*
 * Create options for tables
 */
function makeOptions(time, data, type, tick) {

	var type = type.substring(1);
	var minValue;
	var maxValue;
	var range = maxValue - minValue;

	if (type === "mood") {
		minValue = 0;
		maxValue = 5;
	} else if (type === "feeling") {
		minValue = 0;
		maxValue = 5;
	} else if (type === "weight") {
		minValue = 50;
		maxValue = 150;
	} else if (type === "pulse") {
		minValue = 50;
		maxValue = 200;
	} else if (type === "bmi") {
		minValue = 10;
		maxValue = 30;
	} else if (type === "bloodOxygen") {
		minValue = 90;
		maxValue = 100;
	}

	var milliseconds = time.end - time.start;
	var d, h, m, s;
	s = Math.floor(milliseconds / 1000);
	m = Math.floor(s / 60);
	s = s % 60;
	h = Math.floor(m / 60);
	m = m % 60;
	d = Math.floor(h / 24);
	h = h % 24;
	var timeTick = Number(d / 10);

	return {
		xaxes : [ {
			min : time.start,
			max : (time.end) + 86400000,
			mode : "time",
			timeformat : "%d/%m/%y",
			tickSize : [ timeTick, "day" ]
		} ],
		yaxis : {
			min : minValue,
			max : maxValue,
			tickSize : tick
		},
		series : {
			lines : {
				show : true
			},
			points : {
				show : true
			}
		},
		grid : {
			hoverable : true,
			markings : weekendAreas
		}
	};
}

/*
 * Get startTime and endTime from datepickers
 */
function getStartAndEnd() {
	var startTimeString = $('#startTime').val();
	var endTimeString = $('#endTime').val();
	var start = startTimeString.split("/");
	var end = endTimeString.split("/");
	var startDate = new Date(Number(start[2]), Number(start[1]) - 1,
			Number(start[0]));
	var endDate = new Date(Number(end[2]), Number(end[1]) - 1, Number(end[0]));
	if (startDate.getTime() > endDate.getTime()) {
		alert("Kuupäevad on valepidi sisestatud!");
	}

	return {
		start : startDate.getTime(),
		end : endDate.getTime()
	}
}

/*
 * Get today's date
 */
function getNow() {
	var now = new Date();
	return now.toString("dd/MM/yyyy");
}

/*
 * Get a date 30 days ago from today
 */
function get30DaysAgo() {
	var now = new Date().getTime();
	var then = new Date(now - 2592000000);
	return then.toString("dd/MM/yyyy");
}

/*
 * Parse data
 */
function getDataFromString(string) {
	var results = stringToArray(string);

	var data = [];
	var temp = [];

	while (results.length > 0) {
		temp.push(Number(results.splice(0, 1)));
		temp.push(Number(results.splice(0, 1)));

		data.push(temp);

		temp = [];
	}
	console.log(data);
	return data;
}

/*
 * Parse data
 */
function stringToArray(string) {
	var res1 = string.replace("[", "");
	var res2 = res1.replace("]", "");
	var res3 = res2.replace(/,/g, "");
	var results = res3.replace(/\s{2,}/g, " ");
	results = results.split(" ");
	return results;
}

/*
 * Draw table using measurement data, placeholder as location, range, tick
 */
function drawTable(measurementData, placeholder, minVal, maxVal, tick) {

	var time = getStartAndEnd();

	var data = getDataFromString(measurementData);

	var options = makeOptions(time, data, placeholder, tick);

	$.plot($(placeholder), [ data ], options);
}

/*
 * Return weekend areas to show on tables
 */
function weekendAreas(axes) {

	var markings = [], d = new Date(axes.xaxis.min);

	/*
	 * Go to the first Saturday
	 */
	d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
	d.setUTCSeconds(0);
	d.setUTCMinutes(0);
	d.setUTCHours(0);

	var i = d.getTime();

	do {
		markings.push({
			xaxis : {
				from : i,
				to : i + 2 * 24 * 60 * 60 * 1000
			}
		});
		i += 7 * 24 * 60 * 60 * 1000;
	} while (i < axes.xaxis.max);

	return markings;
}

/*
 * Create data sets for tables with several datalines
 */
function makeDatasets(personData, groupData) {
	
	personData = getDataFromString(personData);

	var groupDataAsJson = JSON.parse(groupData);

	var datasets = {
		"personData" : {
			label : "personData",
			data : personData
		},
	}

	for (var i = 0; i < groupDataAsJson.length; i++) {
		var obj = json[i];
		datasets[obj.name] = {
			label : obj.name,
			data : obj.data
		}
	}

	/*
	var i;
	var gd;
	for (i = 0; i < groupData.length; i += 1) {
		gd = getDataFromString(groupData[i]);
		datasets["person"] = {
			label : "asdf",
			data : gd
		}
	}
	*/
	return datasets;
}

/*
 * TEZT
 */
function test(data1, data2) {
	alert(JSON.parse(data2));
	var datasets = makeDatasets(data1, data2);
	var time = getStartAndEnd();
	var options = makeOptions(time, data1, "mood", 1);
	var i = 0;
	var choiceContainer = $("#choices");

	options.yaxis.min = 0;
	options.yaxis.max = 5;

	$.each(datasets, function(key, val) {
		val.color = i;
		i += 1;
	});

	/*
	 * Insert checkboxes
	 */
	$.each(datasets, function(key, val) {
		choiceContainer.append("<br/><input type='checkbox' name='" + key
				+ "' checked='checked' id='id" + key + "'></input>"
				+ "<label for='id" + key + "'>" + val.label + "</label>");
	});
	choiceContainer.find("input").click(plotAccordingToChoices);

	function plotAccordingToChoices() {
		var datasets = makeDatasets(data1, data2);
		var data = [];
		choiceContainer.find("input:checked").each(function() {
			var key = $(this).attr("name");
			if (key && datasets[key]) {
				data.push(datasets[key]);
			}
		});
		if (data.length > 0) {
			$.plot("#placeholder", data, options);
		}
	}
	plotAccordingToChoices(datasets);
	$("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
}
