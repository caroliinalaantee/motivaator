package com.tieto.geekoff1.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.service.Interfaces.DataImportServiceInterface;
import com.tieto.geekoff1.service.Interfaces.PeopleControllerServiceInterface;


@Service
public class DataImportService implements DataImportServiceInterface {

	@Autowired
    private ApplicationContext appContext;
	//private String resourceUrl = "${device.input.data.url}";
	private String resourceUrl = "http://194.106.118.77/geekoff_data.txt";
	@Autowired
	@Qualifier("peopleControllerService")
	private PeopleControllerServiceInterface peopleService;
	
	public List<String> readFromResourceAndTokenizeToMeasurements(String resourceLocation) {

    	List<String> measurements = new ArrayList<String>();
    	
    	    	Resource resource = 
    	    			appContext.getResource(resourceUrl);
    	 
    	    try{
    	     	  InputStream is = resource.getInputStream();
    	          BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
    	 
    	         
    	          String line;
    	          
    	          while ((line = br.readLine()) != null) {
    	            
    	             StringTokenizer stringTokenizer = new StringTokenizer(line, ",");
    	             
    	    	  		while (stringTokenizer.hasMoreElements()) {
    	    	  			
    	    	  			String measurement = stringTokenizer.nextToken();
    	    	  			measurements.add(measurement);    	    	  			
    	    	  		}
    	       	  
    	          } 
    	          br.close();
    	 
    	    	}catch(IOException e){
    	    		e.printStackTrace();
    	    	}
    	    
    	    	
		return measurements;
	}
	
	

	
	
	public List<List<String>> separateMeasurementSets(
			List<String> allMeasurements) {
		
		List<String> measurementSet = null;
		List<List<String>> measurementSets = new ArrayList<List<String>>();
		
			for(String measurement:allMeasurements){
				
				if(isThisADate(measurement)){
					
					if(measurementSet != null){
						measurementSets.add(measurementSet);
						
					} 
					
					measurementSet = new ArrayList<String>();
					measurementSet.add(measurement);			
					
				} 
				
				else if (!isThisADate(measurement) && measurementSet != null){
					measurementSet.add(measurement);
					}
				
			}
			measurementSets.add(measurementSet);
			
			
			
		return measurementSets;
	}

	
	
	public Map<String, String> separateOneMeasurementSetToMeasurement(
			List<String> measurements) {
		
		Map<String,String> measurementMap = new HashMap<String,String>();
		String key;
		String value;
		
		
		for(String measurement:measurements){
			
			if(isThisADate(measurement)){
				parseMeasurementAndPutToMap("DATETIME",measurement,measurement,measurementMap);
			} 
			else if(measurement.equals("unconfirmed")){
				// do nothing;
			} 
			else {
				
				StringTokenizer stringTokenizer = new StringTokenizer(measurement, "=");
				key = stringTokenizer.nextToken();
				value = stringTokenizer.nextToken();
				parseMeasurementAndPutToMap(key,value,measurement,measurementMap);
			}
			
		}
		
		if(measurementMap.get("personId")==null){
			measurementMap = null;
			
		} 
		
		
		return measurementMap;
	}

	private void parseMeasurementAndPutToMap(String key, String value, String measurement,
			Map<String, String> measurementMap) {
		
		if (key.equals("DATETIME")){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			try {
				Date dateTime = sdf.parse(measurement);
				long dateTimeMilliseconds =dateTime.getTime();
				String dateTimeMillisecondsString = Long.toString(dateTimeMilliseconds); 
				measurementMap.put("measureDate", dateTimeMillisecondsString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} else if (key.equals("id")){
			StringTokenizer stringTokenizer = new StringTokenizer(value, "_");
			String personId = stringTokenizer.nextToken();
			String firstName = stringTokenizer.nextToken();
			System.out.println("Hello from parser,       personId =     "+ personId);
			System.out.println("Hello from parser,       firstName =     "+ firstName);
			
			measurementMap.put("personId", personId);
			measurementMap.put("firstName", firstName);
		} else if (key.equals("mood")){
			StringTokenizer stringTokenizer = new StringTokenizer(measurement, "=");
			stringTokenizer.nextToken();
			stringTokenizer.nextToken();
			String moodPercent = stringTokenizer.nextToken();
			moodPercent = moodPercent.substring(0, moodPercent.length()-1);
			int mood = Integer.parseInt(moodPercent)/20;
			moodPercent = Integer.toString(mood);
			measurementMap.put("mood", moodPercent);
		} else if (key.equals("feel")){
			StringTokenizer stringTokenizer = new StringTokenizer(measurement, "=");
			stringTokenizer.nextToken();
			stringTokenizer.nextToken();
			String feelPercent = stringTokenizer.nextToken();
			feelPercent = feelPercent.substring(0, feelPercent.length()-1);
			int mood = Integer.parseInt(feelPercent)/20;
			feelPercent = Integer.toString(mood);
			measurementMap.put("feel", feelPercent);
		} else if (key.equals("ppg_pulse")){
			value = value.substring(0, value.length()-4);
			measurementMap.put("ppgPulse", value);
		} else if (key.equals("ppg_spo2")){
			value = value.substring(0, value.length()-1);
			measurementMap.put("spo2", value);
		} else if (key.equals("sys")){
			measurementMap.put(key, value);
		} else if (key.equals("dia")){
			measurementMap.put(key, value);
		} else if (key.equals("pulse")){
			measurementMap.put(key, value);
		} else if (key.equals("weight")){
			measurementMap.put(key, value);
		} else if (key.equals("height")){
			measurementMap.put(key, value);
		} else if (key.equals("bmi")){
			measurementMap.put(key, value);
		} 
		
	}





	public MeasurementsEntity createMeasurementsEntityFromData(
			Map<String, String> measurements) {
		
		
		
		if (measurements == null){
			return null;
		} 
		
		String measureDate = measurements.get("measureDate");
		
		/*if(measurements.get("measureDate")){
			return null;
		}*/
		
		String personId = measurements.get("personId");
		
		String firstName = measurements.get("firstName");
		
		//System.out.println("Hello from createMeasurementsEntityFromData,       personId =     "+ personId);
		//System.out.println("Hello from pcreateMeasurementsEntityFromData,       firstName =     "+ firstName);
		
		PersonsEntity person = peopleService.getPersonByIdAndName(personId, firstName);
		
		//System.out.println("Hello from createMeasurementsEntityFromData @Person,       personId =     "+ person.getPersonId());
		//System.out.println("Hello from pcreateMeasurementsEntityFromData @Person,       firstName =     "+ person.getFirstName());
		MeasurementsEntity measurement = new MeasurementsEntity();
		
		measurement.setBmi(measurements.get("bmi"));
		measurement.setDeviceId("Eliko");
		measurement.setDia(measurements.get("dia"));
		measurement.setFeel(measurements.get("feel"));
		measurement.setHeight(measurements.get("height"));
		measurement.setInsertPersonsEntity(person);
		measurement.setMeasureDate(measurements.get("measureDate"));
		measurement.setMood(measurements.get("mood"));
		measurement.setPpgPulse(measurements.get("ppgPulse"));
		measurement.setPulse(measurements.get("pulse"));
		measurement.setSpo2(measurements.get("spo2"));
		measurement.setSys(measurements.get("sys"));
		measurement.setWeight(measurements.get("weight"));
		
		
		/*if(measurements.get("measureDate")){
			
		}*/
		
		
		/*String mm = measurement.getBmi();
		System.out.println("BMI= "+mm);
		
		mm = measurement.getDeviceId();
		System.out.println("DeviceId=     "+mm);
		
		mm = measurement.getDia();
		System.out.println("Dia=     "+mm);
		
		mm = measurement.getFeel();
		System.out.println("Feel=     "+mm);
		
		
		mm = measurement.getHeight();
		System.out.println("Height=     "+mm);
		
		mm = measurement.getMeasureDate();
		System.out.println("Date=     "+mm);
		
		mm = measurement.getMood();
		System.out.println("Mood=     "+mm);
		
		mm = measurement.getPpgPulse();
		System.out.println("ppgPulse=     "+mm);
		
		mm = measurement.getPulse();
		System.out.println("pulse=     "+mm);
		
		mm = measurement.getSpo2();
		System.out.println("spo2=     "+mm);
		
		mm = measurement.getSys();
		System.out.println("sys=     "+mm);
		
		mm = measurement.getWeight();
		System.out.println("weight=     "+mm)*/
		
		//mm = measurement.getInsertPersonsEntity();
		
		return measurement;
	}
	
	
	
	private boolean isThisADate(String measurement){
		
		Pattern p = Pattern.compile("(\\d)(\\d)(\\d)(\\d)(-)(\\d)(\\d)(-)(\\d)(\\d)"); // the expression
		Matcher m = p.matcher(measurement); // the source
		while(m.find()){ 
			return true;
		}
			
		
		return false;
	}




	public boolean startDataImport() {
		boolean success = false;
		List<String> allMeasurements = this.readFromResourceAndTokenizeToMeasurements(null);
    	List<List<String>> measurementSets = this.separateMeasurementSets(allMeasurements);
    	
    	for (List<String> measurements: measurementSets){
    		
    		Map <String,String> measurementPairs = this.separateOneMeasurementSetToMeasurement(measurements);
    		MeasurementsEntity measurement = this.createMeasurementsEntityFromData(measurementPairs);
    		
    		if(measurement!=null){
    			PersonsEntity person = measurement.getInsertPersonsEntity();
        		peopleService.saveMeasurements(person, measurement);
    		}
    		
    		
    	}
    	success = true;
		return success;
	}
	
	
	

}
