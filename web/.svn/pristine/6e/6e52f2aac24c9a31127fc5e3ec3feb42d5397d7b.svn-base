package com.tieto.geekoff1.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.PeopleControllerServiceInterface;


@Service("peopleControllerService")
@Transactional
public class PeopleControllerService implements PeopleControllerServiceInterface {
	
	static Random random = new Random();
	//static List<PersonWithMeasurementsDTO> allPersonsWithMeasurements = new ArrayList<PersonWithMeasurementsDTO>();
	static int numberOfDays = 30;
	
	/* (non-Javadoc)
	 * @see com.tieto.geekoff1.service.PeopleControllerServiceInterface#getPersonByNameAndFamilyName(java.lang.String, java.lang.String)
	 */
	public PersonDTO getPersonByNameAndFamilyName(String name, String lastName) {
		List<PersonDTO> persons = getAllPersons();
		PersonDTO p = null;
		for (PersonDTO person:persons) {
			if (person.getFirstName().equals(name) && person.getLastName().equals(lastName)) {
				p = person;
			}
		}
		return p;
	}
	
	/* (non-Javadoc)
	 * @see com.tieto.geekoff1.service.PeopleControllerServiceInterface#getAllPersons()
	 */
	public List<PersonDTO> getAllPersons() {
		
		List<PersonDTO> persons = new ArrayList<PersonDTO>();
		
		PersonDTO person1 = new PersonDTO();
		PersonDTO person2 = new PersonDTO();
		PersonDTO person3 = new PersonDTO();
		PersonDTO person4 = new PersonDTO();
		
		person1.setFirstName("Vello");
		person1.setLastName("Viiuldaja");
		person1.setIdNumber("588111222");
		persons.add(person1);
		
		person2.setFirstName("Mari");
		person2.setLastName("Maasikas");
		person2.setIdNumber("381234567");
		persons.add(person2);
		
		person3.setFirstName("Reet");
		person3.setLastName("Redis");
		person3.setIdNumber("470707070");
		persons.add(person3);
		
		person4.setFirstName("Jaan");
		person4.setLastName("Jalgratas");
		person4.setIdNumber("373737373");
		persons.add(person4);
		
		return  persons;
	}

	public PersonWithMeasurementsDTO getPersonWithMeasurementsByName(
			String firstName, String lastName) {
		
		PersonWithMeasurementsDTO personWithMM = new PersonWithMeasurementsDTO();
		
		PersonDTO person = this.getPersonByNameAndFamilyName(firstName, lastName);
		MeasurementsDTO measurements = this.getAllMeasurementsByPerson(person);
		personWithMM.setPerson(person);
		personWithMM.setMeasurements(measurements);
		
		return personWithMM;
	}
	

	public MeasurementsDTO getAllMeasurementsByPerson(PersonDTO person) {
		
		MeasurementsDTO measurements = this.makeRandomMeasurements();
		
		return measurements;
	}
	
	
	////// Dummy measurements come from here  -------------------------------
	
	private MeasurementsDTO makeRandomMeasurements() {
		List<MeasurementDTO> mood = getRandomMoodList();
		List<MeasurementDTO> feeling = getRandomFeelingList();
		
		MeasurementsDTO measurements = new MeasurementsDTO();
		measurements.setFeeling(feeling);
		measurements.setMood(mood);

		return measurements;
	}

	private List<MeasurementDTO> getRandomMoodList() {
		List<MeasurementDTO> moodList =  new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO mood = new MeasurementDTO();
			mood.setValue(random.nextInt(10) + 1);
			mood.setDate(new Date(2014, 8, i + 1));
			mood.setUnit("percent");
			moodList.add(mood);
		}
		return moodList;
	}
	
	private List<MeasurementDTO> getRandomFeelingList() {
		List<MeasurementDTO> feelingList = new ArrayList<MeasurementDTO>();
		for (int i = 0; i < numberOfDays; i += 1) {
			MeasurementDTO feeling = new MeasurementDTO();
			feeling.setValue(random.nextInt(10) + 1);
			feeling.setDate(new Date(2014, 8, i + 1));
			feeling.setUnit("percent");
			feelingList.add(feeling);
		}
		return feelingList;
	}
	
	//------//////////////////---------------//////////////////////////////////////////////////////////
	
	

}
