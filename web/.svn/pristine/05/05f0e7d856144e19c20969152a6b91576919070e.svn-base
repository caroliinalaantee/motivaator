package com.tieto.geekoff1.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.GroupsEntity;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;

@Component
public class PeopleDAO implements PeopleDaoInterface {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private Logger logger;

	public PersonsEntity getPersonByNameAndFamilyName(String name,
			String lastName) {
		logger.debug("hello from PeopleDAO. Activated getPersonByNameAndFamilyName name to search is "
				+ name + " " + lastName);

		PersonsEntity personEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			personEntity = (PersonsEntity) session
					.createCriteria(PersonsEntity.class)
					.add(Restrictions.eq("firstName", name))
					.add(Restrictions.eq("lastName", lastName)).uniqueResult();
		} catch (HibernateException e) {
			logger.error("Unable to complete PeopleDAO getPersonByNameAndFamilyName"
					+ e.getMessage());
		}

		return personEntity;

	}

	public PersonsEntity getPersonByIdAndName(String id, String name) {
		logger.debug("hello from PeopleDAO. Activated getPersonByIdAndName search parameters are "
				+ id + " " + name);

		PersonsEntity personEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			personEntity = (PersonsEntity) session
					.createCriteria(PersonsEntity.class)
					.add(Restrictions.eq("personId", id))
					.add(Restrictions.eq("firstName", name)).uniqueResult();
		} catch (HibernateException e) {
			logger.error("Unable to complete PeopleDAO getPersonByIdAndName"
					+ e.getMessage());
		}

		return personEntity;
	}

	@SuppressWarnings("unchecked")
	public List<PersonsEntity> getAllPersons() {
		logger.debug("hello from PeopleDAO. Activated getAllPersons, getting all persons List ");

		Set<PersonsEntity> personsEntitySet = new HashSet<PersonsEntity>();
		List<PersonsEntity> personsEntityList = new ArrayList<PersonsEntity>();
		List<PersonsEntity> personsEntityListNew = new ArrayList<PersonsEntity>();

		Session session = sessionFactory.getCurrentSession();
		try {
			personsEntityList = (List<PersonsEntity>) (session
					.createCriteria(PersonsEntity.class).list());
		} catch (HibernateException e) {
			logger.error("Unable to complete PeopleDAO getAllPersons"
					+ e.getMessage());
		}

		// list-> set -> list + sort conversion
		personsEntitySet = new HashSet<PersonsEntity>(personsEntityList);
		personsEntityListNew = new ArrayList<PersonsEntity>(personsEntitySet);
		Collections.sort(personsEntityListNew);
		return personsEntityListNew;
	}

	public List<MeasurementsEntity> getPersonMeasurementsByPersonName(
			String name, String lastName) {
		logger.debug("hello from PeopleDAO. Activated getPersonMeasurementsByPersonName name to search is "
				+ name + " " + lastName);

		PersonsEntity person = getPersonByNameAndFamilyName(name, lastName);
		Set<MeasurementsEntity> personMeasurementsSet = new HashSet<MeasurementsEntity>();

		if (person == null) {
			return null;
		}
		// set -> list + sort
		personMeasurementsSet = person.getMeasurementsList();
		List<MeasurementsEntity> personMeasurementsList = new ArrayList<MeasurementsEntity>(
				personMeasurementsSet);
		Collections.sort(personMeasurementsList);
		return personMeasurementsList;
	}

	public void saveNewPerson(PersonsEntity person) {
		logger.debug("hello from PeopleDAO. Activated saveNewPerson. Saving new person to the database");
		Session session = sessionFactory.getCurrentSession();
		session.save(person);
	}

	public void saveMeasurementToExistingPerson(PersonsEntity person,
			MeasurementsEntity measurement) {
		logger.debug("hello from PeopleDAO. Activated saveMeasurementToExistingPerson. Saving measurements to the database. Attaching them to existing person");
		Session session = sessionFactory.getCurrentSession();

		person.getMeasurementsList().add(measurement);
		measurement.setInsertPersonsEntity(person);
		session.save(measurement);
		session.update(person);

	}

	public void saveExistingGroupToPerson(PersonsEntity person,
			GroupsEntity group) {
		logger.debug("hello from PeopleDAO. Activated saveExistingGroupToPerson. Adding group to the person");
		Session session = sessionFactory.getCurrentSession();

		if (person == null) {
			logger.debug("person on null");
		}
		if (group == null) {
			logger.debug("group on null");
		}

		person.getGroups().add(group);
		group.getPersonsList().add(person);

		session.update(group);
		session.update(person);
	}

	public MeasurementsEntity getMeasurementsEntityByDateTime(String dateTime) {
		logger.debug("hello from PeopleDAO. Activated getMeasurementsEntityByDateTime. Getting MeasurementsEntity by Datetime: "
				+ dateTime);
		MeasurementsEntity MeasurementsEntity = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			MeasurementsEntity = (MeasurementsEntity) session
					.createCriteria(MeasurementsEntity.class)
					.add(Restrictions.eq("measureDate", dateTime))
					.uniqueResult();
		} catch (HibernateException e) {
			logger.error("Unable to complete PeopleDAO getMeasurementsEntityByDateTime"
					+ e.getMessage());
		}
		return MeasurementsEntity;
	}

	public void saveOrUpdateCommentToPerson(String id, String name,
			String comment) {
		logger.debug("hello from PeopleDAO. Activated saveOrUpdateCommentToPerson. Saving or Updating a comment to person: "
				+ id + " " + name + "comment is:" + comment);
		PersonsEntity person = getPersonByIdAndName(id, name);
		person.setComment(comment);
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(person);
	}

	public void saveOrUpdateLastName(String id, String name, String lastName) {
		logger.debug("hello from PeopleDAO. Activated saveOrUpdateLastName. Saving or Updating a lastname of person: "
				+ id + " " + name + "lastname to update is:" + lastName);
		PersonsEntity person = getPersonByIdAndName(id, name);
		person.setLastName(lastName);
		Session session = sessionFactory.getCurrentSession();
		session.update(person);
	}
}
