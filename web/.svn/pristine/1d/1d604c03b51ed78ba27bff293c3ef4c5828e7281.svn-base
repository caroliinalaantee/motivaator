package com.tieto.geekoff1.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "PERSON")
public class PersonsEntity implements Comparable {

	@Id
	@Column(name = "P_ID")
	@GeneratedValue
	private long Id;

	/* isikuandmed */
	@Column(name = "PERSON_ID")
	private String personId; // 3890608****

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName; // FAILIST EI SAA

	@Lob
	@Column(name = "COMMENT")
	private String comment;

	@OneToMany(mappedBy = "insertPersonsEntity", fetch = FetchType.EAGER)
	private Set<MeasurementsEntity> measurementsList = new HashSet<MeasurementsEntity>();

	@ManyToMany(mappedBy = "personsList", cascade = CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<GroupsEntity> groups = new ArrayList<GroupsEntity>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonsEntity other = (PersonsEntity) obj;
		if (Id != other.Id)
			return false;
		return true;
	}

	public List<String> getGroupNames() {
		List<String> groupNames = new ArrayList<String>();
		for (GroupsEntity entity : this.groups) {
			groupNames.add(entity.getGroupName());
		}
		return groupNames;
	}

	public Set<MeasurementsEntity> getMeasurementsList() {
		return measurementsList;
	}

	public void setMeasurementsList(Set<MeasurementsEntity> measurementsList) {
		this.measurementsList = measurementsList;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String surName) {
		this.lastName = surName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<GroupsEntity> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupsEntity> groups) {
		this.groups = groups;
	}

	@Override
	public String toString() {
		String string = "person entity: ";
		if (this.firstName == null) {
			string += "first name is NULL ";
		}
		if (this.lastName == null) {
			string += "lastname is NULL";
		}
		if (this.groups == null) {
			string += "groups list is NULL";
		}
		if (this.personId == null) {
			string += "id is NULL";
		}
		return string;
	}

	public int compareTo(Object person) {
		return -1
				* ((PersonsEntity) person).getFirstName().compareTo(
						this.getFirstName());
	}
}
