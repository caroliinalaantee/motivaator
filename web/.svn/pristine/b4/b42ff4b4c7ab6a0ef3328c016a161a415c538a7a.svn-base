package com.tieto.geekoff1.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.service.Interfaces.PeopleServiceInterface;

@Controller
public class ChangeLastnameController {

	@Autowired
	@Qualifier("peopleControllerService")
	private PeopleServiceInterface peopleService;
	@Autowired
	private Logger logger;

	@RequestMapping(value = "/changeName", method = RequestMethod.GET)
	public String addLabel(
			@RequestHeader("Referer") String referer,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "lastName", required = true) String lastName,
			@RequestParam(value = "firstName", required = true) String firstName,
			Model model) {
		if (id == null && lastName == null) {
			logger.error("Problem in AddLabelController, id or lastName are null");
		}
		logger.debug("Hello from ChangeLastnameController. Will use peopleService.saveLastNametoPerson. Entering in:"
				+ id + " " + firstName + " " + lastName);
		peopleService.saveLastNameToPerson(id, firstName, lastName);

		model.addAttribute("namesList", getNameList());

		return "people";
	}

	public List<String> getNameList() {
		List<PersonDTO> people = peopleService.getAllPersons();
		List<String> names = new ArrayList<String>();
		for (PersonDTO p : people) {
			names.add(getFullname(p));
		}
		return names;
	}

	public String getFullname(PersonDTO person) {
		return person.getFirstName() + " " + person.getLastName();
	}

}