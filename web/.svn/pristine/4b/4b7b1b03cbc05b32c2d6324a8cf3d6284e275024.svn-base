package com.tieto.geekoff1.dao;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.utils.PropertiesReader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/com/tieto/dao/applicationContext.xml")
@TransactionConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class PeopleDaoTest extends
		AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private PeopleDaoInterface peopleDao;

	@Test
	public void testInsertNewPerson() {
		PersonsEntity person = new PersonsEntity();
		person.setFirstName(PropertiesReader.getTest("nameN"));
		person.setLastName(PropertiesReader.getTest("lastNameN"));
		person.setPersonId(PropertiesReader.getTest("idN"));
		peopleDao.saveNewPerson(person);
		assertEquals(
				person,
				peopleDao.getPersonByIdAndName(person.getPersonId(),
						person.getFirstName()));
	}

	@Test
	public void testInsertExistingPerson() {
		PersonsEntity person = new PersonsEntity();
		person.setFirstName(PropertiesReader.getTest("nameE"));
		person.setLastName(PropertiesReader.getTest("lastNameE"));
		person.setPersonId(PropertiesReader.getTest("idE"));
		peopleDao.saveNewPerson(person);
	}

	PersonsEntity testDummyPerson = new PersonsEntity();
	MeasurementsEntity testDummyMeasurement = new MeasurementsEntity();
	PersonsEntity retrievedDummyPerson = new PersonsEntity();

	private String testDummyFirstName = PropertiesReader.getTest("name");
	private String testDummyLastName = "Teadmata";
	private String testDummyId = "3890608****";
	private String testFeel = "2";

	public PersonsEntity getTestPersonByName(String firstName, String lastName) {
		testDummyPerson = peopleDao.getPersonByNameAndFamilyName(
				testDummyFirstName, testDummyLastName);
		return testDummyPerson;
	}

	public MeasurementsEntity setTestPersonNewMeasurements() {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -1);

		testDummyMeasurement.setFeel(testFeel);
		testDummyMeasurement.setMood(String.valueOf((int) (Math.random() * 6)));
		testDummyMeasurement
				.setDia(String.valueOf((int) (Math.random() * 50 + 50)));
		testDummyMeasurement
				.setSys(String.valueOf((int) (Math.random() * 50 + 80)));
		testDummyMeasurement
				.setSpo2(String.valueOf((int) (Math.random() * 10 + 85)));
		testDummyMeasurement.setMeasureDate(cal.getTime().getTime() + "");
		testDummyMeasurement
				.setPulse(String.valueOf((int) (Math.random() * 50 + 90)));
		testDummyMeasurement
				.setWeight(String.valueOf((int) (Math.random() * 5 + 70)));
		testDummyMeasurement.setHeight(String.valueOf(170));
		testDummyMeasurement
				.setBmi(String.valueOf((int) (Math.random() * 2 + 23)));
		testDummyMeasurement.setDeviceId("Eliko");
		return testDummyMeasurement;
	}

	@Test
	// @Rollback
	public void testInsertNewMeasurementToTestPerson() {
		// get person
		testDummyPerson = getTestPersonByName(testDummyFirstName,
				testDummyLastName);
		// get list Size
		int listSizeBefore = testDummyPerson.getMeasurementsList().size();
		// add measurements
		setTestPersonNewMeasurements();
		// save
		peopleDao.saveMeasurementToExistingPerson(testDummyPerson,
				testDummyMeasurement);
		// get updated personObject
		retrievedDummyPerson = peopleDao.getPersonByNameAndFamilyName(
				testDummyFirstName, testDummyLastName);
		// get list Size
		int listSizeAfter = retrievedDummyPerson.getMeasurementsList().size();
		// check before +1 = after
		Assert.assertEquals(listSizeBefore + 1, listSizeAfter);

	}

	@Test
	public void testCheckFeelInsertedAndRetrieved() {
		// get person
		retrievedDummyPerson = getTestPersonByName(testDummyFirstName,
				testDummyLastName);
		// add measurements
		setTestPersonNewMeasurements();
		// save
		peopleDao.saveMeasurementToExistingPerson(testDummyPerson,
				testDummyMeasurement);
		// get personObjects measurements list
		retrievedDummyPerson = peopleDao.getPersonByNameAndFamilyName(
				testDummyFirstName, testDummyLastName);
		Set<MeasurementsEntity> measurements = retrievedDummyPerson
				.getMeasurementsList();
		// get measurement
		// String feel = measurements.get(17).getFeel();
		String feel = "10";
		// check

		Assert.assertEquals(testFeel, feel);
	}

}
