package com.tieto.geekoff1.helloservices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tieto.geekoff1.dao.interfaces.PeopleDaoInterface;
import com.tieto.geekoff1.domain.MeasurementsEntity;
import com.tieto.geekoff1.domain.PersonsEntity;
import com.tieto.geekoff1.dto.MeasurementDTO;
import com.tieto.geekoff1.dto.MeasurementsDTO;
import com.tieto.geekoff1.dto.PersonDTO;
import com.tieto.geekoff1.dto.PersonWithMeasurementsDTO;
import com.tieto.geekoff1.service.Interfaces.PeopleControllerServiceInterface;

@Service("peopleControllerServiceVer2")
@Transactional
public class PeopleControllerServiceDbLauriVer2 implements PeopleControllerServiceInterface {

	
	@Autowired
	private PeopleDaoInterface peopleDao;
	
	
	public PersonDTO getPersonByNameAndFamilyName(String name, String lastName) {
		
		PersonDTO person = new PersonDTO();
		PersonsEntity personsEntity = peopleDao.getPersonByNameAndFamilyName(name, lastName);
		
		person.setFirstName(personsEntity.getFirstName());
		person.setLastName(personsEntity.getLastName());
		person.setIdNumber(personsEntity.getPersonId());		
		
		return person;
	}

	
	public List<PersonDTO> getAllPersons() {		
		
		List <PersonDTO> persons = new ArrayList<PersonDTO>();
		List <PersonsEntity> personsEntityList = peopleDao.getAllPersons();
		PersonDTO person;
		
		for (PersonsEntity personsEntity : personsEntityList){
			person = new PersonDTO();
			person.setFirstName(personsEntity.getFirstName());
			person.setLastName(personsEntity.getLastName());
			person.setIdNumber(personsEntity.getPersonId());
			persons.add(person);
		}
		
		return persons;
	}
	
	
	
	
	
	
	public PersonWithMeasurementsDTO getPersonWithMeasurementsByName(
			String firstName, String lastName) {
		
		PersonWithMeasurementsDTO personWithMM = new PersonWithMeasurementsDTO();
		
		PersonDTO person = this.getPersonByNameAndFamilyName(firstName, lastName);
		MeasurementsDTO measurements = this.getAllMeasurementsByPerson(person);
		personWithMM.setPerson(person);
		personWithMM.setMeasurements(measurements);
		
		return personWithMM;
		
	}
	

	public MeasurementsDTO getAllMeasurementsByPerson(PersonDTO person) {
		
		MeasurementsDTO measurementsDTO = new MeasurementsDTO();
		List<MeasurementsEntity> measurementsEntityList = 
				peopleDao.getPersonMeasurementsByPersonName(person.getFirstName(), person.getLastName());
		
		for (MeasurementsEntity measurementsEntity:measurementsEntityList){
			String date = measurementsEntity.getMeasureDate();
			
			String feeling = measurementsEntity.getFeel();
			this.addMeasurementEntitysToMeasurementsDTO("feeling", feeling, date, measurementsDTO);
			
			String mood = measurementsEntity.getMood();
			this.addMeasurementEntitysToMeasurementsDTO("mood", mood, date, measurementsDTO);
			
			String weight = measurementsEntity.getWeight();
			this.addMeasurementEntitysToMeasurementsDTO("weight", weight, date, measurementsDTO);
			
			String pulse = measurementsEntity.getPulse();
			this.addMeasurementEntitysToMeasurementsDTO("pulse", pulse, date, measurementsDTO);
			
			String bloodOxygen = measurementsEntity.getSpo2();
			this.addMeasurementEntitysToMeasurementsDTO("bloodOxygen", bloodOxygen, date, measurementsDTO);
			
			String height = measurementsEntity.getHeight();
			this.addMeasurementEntitysToMeasurementsDTO("height", feeling, date, measurementsDTO);
			
			String upperBloodPressure = measurementsEntity.getSys();
			this.addMeasurementEntitysToMeasurementsDTO("upperBloodPressure", upperBloodPressure, date, measurementsDTO);
			
			String lowerBloodPressure = measurementsEntity.getDia();
			this.addMeasurementEntitysToMeasurementsDTO("lowerBloodPressure", lowerBloodPressure, date, measurementsDTO);
			
			String bmi = measurementsEntity.getBmi();
			this.addMeasurementEntitysToMeasurementsDTO("bmi", bmi, date, measurementsDTO);
				
		}	
		return measurementsDTO;
	}
	
	private void addMeasurementEntitysToMeasurementsDTO(String devicename, String measurement, String date, MeasurementsDTO measurementsDTO){
		if (devicename.equals("feeling") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getFeeling().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("mood") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getMood().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("weight") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getWeight().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("pulse") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getPulse().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("bloodOxygen") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getBloodOxygen().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("height") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getHeight().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("upperBloodPressure") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getUpperBloodPressure().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("lowerBloodPressure") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getLowerBloodPressure().add(singleMeasurementDTO);
		}
		
		else if (devicename.equals("bmi") && measurement!= null){
			MeasurementDTO singleMeasurementDTO = new MeasurementDTO();
			singleMeasurementDTO.setDate(date);
			singleMeasurementDTO.setValue(measurement);
			measurementsDTO.getBmi().add(singleMeasurementDTO);
		}
		
		
	}


	public PersonsEntity createNewPerson(String personId, String firstName) {
		// TODO Auto-generated method stub
		return null;
	}


	public PersonsEntity getPersonByIdAndName(String personId, String firstName) {
		// TODO Auto-generated method stub
		return null;
	}


	public void saveMeasurements(PersonsEntity person,
			MeasurementsEntity measurements) {
		// TODO Auto-generated method stub
		
	}





	public void addLabelToPerson(String personId, String firstName, String label) {
		// TODO Auto-generated method stub
		
	}


	public void addGroupToPerson(String personId, String firstName, String label) {
		// TODO Auto-generated method stub
		
	}


	public void createNewGroup(String label) {
		// TODO Auto-generated method stub
		
	}


	public PersonDTO getPersonDTOByFirstNameAndId(String firstName,
			String personId) {
		// TODO Auto-generated method stub
		return null;
	}
	

	
	
}
